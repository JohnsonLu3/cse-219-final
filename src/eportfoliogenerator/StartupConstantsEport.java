package eportfoliogenerator;

/**
 *
 * @author JL
 */
public class StartupConstantsEport {

    public static String SLASH = "/";
    public static String IMAGEPATH = "./Images/";
    public static String SAVEPATH = "./data/ePortFolio/";
    public static String RENDERPATH = "./data/temp/";
    public static String EXPORTPATH = "./Sites/";
    public static String ICONPATH = IMAGEPATH + "Icons/";
    public static String CSSPATH = "/Style/";
    public static String UICSSPATH = CSSPATH + "ePort.css";
    public static String POPUPCSSPATH = CSSPATH + "PopUp.css";

    ///////////////////
    //ICONS
    ////////
    //File Icons
    public static String NEWFILEICONPATH = ICONPATH + "newFile.png";
    public static String SAVEICONPATH = ICONPATH + "save.png";
    public static String SAVEASICONPATH = ICONPATH + "saveAs.png";
    public static String OPENICONPATH = ICONPATH + "openFile.png";
    public static String EXPORTICONPATH = ICONPATH + "export.png";
    public static String EXITICONPATH = ICONPATH + "exit.png";

    ////////
    //Page Edit Icon
    public static String ADDICONPATH = ICONPATH + "add.png";
    public static String DELETEICONPATH = ICONPATH + "delete.png";
    public static String DOWNICONPATH = ICONPATH + "down.png";

    public static String LEFTICONPATH = ICONPATH + "left.png";
    public static String RIGHTICONPATH = ICONPATH + "right.png";

    public static String SELECTICONPATH = ICONPATH + "selectPage.png";
    public static String UPICONPATH = ICONPATH + "up.png";

    ////////
    //WorkSpace Edit Icons
    public static String LAYOUTICONPATH = ICONPATH + "layout.png";
    public static String COLORICONPATH = ICONPATH + "color.png";
    public static String FONTICONPATH = ICONPATH + "font.png";
    public static String TEXTICONPATH = ICONPATH + "textEdit.png";
    public static String IMGICONPATH = ICONPATH + "image.png";
    public static String SLIDEICONPATH = ICONPATH + "slideshow.png";
    public static String VIDEOICONPATH = ICONPATH + "video.png";
    public static String LINKICONPATH = ICONPATH + "link.png";
    public static String EDITICONPATH = ICONPATH + "edit.png";
    public static String REMOVEICONPATH = ICONPATH + "remove.png";

    //CSS Style Sheet classes
    public static String CSS_FILETOOLBAR = "fileToolBar_view";
    public static String CSS_PAGEPANE = "pagePane";
    public static String CSS_SITEPANE = "sitePane";
    public static String CSS_WORKSPACE = "workSpace";
    public static String CSS_PAGEEDITOR = "pageEditor";
    public static String CSS_EDITORHEADER = "editorHeader";
    public static String CSS_COMPONENT = "component";
    public static String CSS_COMPONENTTYPE = "componentType";
    public static String CSS_COMPONENTCONT = "componentCont";
    public static String CSS_SELECTEDCOMP = "selectedComponent";
    
    //PopUp
    public static String CSS_HEADER = "header";
    public static String CSS_RADIO = "radio";
    public static String CSS_RADIOLABEL = "radioLabel";
    public static String CSS_TEXTFIELD = "textField";
    public static String CSS_COMFRIMBUTTONS = "comfrimButtons";
}
