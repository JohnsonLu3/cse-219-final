package Controller;

import static Controller.FileController.EDITED;
import static Controller.FileController.SAVED;
import static Controller.FileController.saveFileName;
import static Controller.SiteTbController.pageCount;
import Page.Page;
import static Page.PageList.checkBoxList;
import static Page.PageList.componentMap;
import static Page.PageList.pageComp;
import static Page.PageList.pageData;
import static Page.PageList.pageList;
import static View.Eport_view.primaryStage;
import View.FileToolbar_view;
import static View.FileToolbar_view.exportButton;
import static View.FileToolbar_view.saveAsButton;
import static View.FileToolbar_view.saveButton;
import static View.SiteEditorView.pageTabs;
import static View.SiteToolbar_view.addPageButt;
import static View.SiteToolbar_view.removePageButt;
import static View.SiteToolbar_view.selectPageButt;
import static View.WorkspaceToolBar_view.bannerImage;
import static View.WorkspaceToolBar_view.bannerTxtField;
import static View.WorkspaceToolBar_view.colorButt;
import static View.WorkspaceToolBar_view.editButt;
import static View.WorkspaceToolBar_view.fontButt;
import static View.WorkspaceToolBar_view.footer;
import static View.WorkspaceToolBar_view.imgButt;
import static View.WorkspaceToolBar_view.layoutButt;
import static View.WorkspaceToolBar_view.linkButt;
import static View.WorkspaceToolBar_view.pageTitleField;
import static View.WorkspaceToolBar_view.removeButt;
import static View.WorkspaceToolBar_view.slideButt;
import static View.WorkspaceToolBar_view.textButt;
import static View.WorkspaceToolBar_view.videoButt;
import static eportfoliogenerator.StartupConstantsEport.SAVEPATH;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author JL
 */
public class FileTbController {

    FileToolbar_view FileTB;
    FileController fController = new FileController();
    private static boolean firstNew = true;

    public FileTbController(FileToolbar_view FTBV) {
        FileTB = FTBV;
        initFileHandlers();
    }

    private void initFileHandlers() {

        /**
         * New Page Button Handler Creates a Eport and a starter Indexpage that
         * houses a scroll pane and VBox for components Creates a componentMap
         * to track all components and their VBoxes Creats a pageData map that
         * tracks the tap and their page info
         */
        FileTB.newButton.setOnAction(e -> {

            if (firstNew == true) {
                makeNewEport();
                //initSelectedTab();
            } else {
                if (EDITED.get() == true) {
                    if (promptToSave() == true) {

                        makeNewEport();

                    }
                }else{
                    makeNewEport();
                }
            }

        });

        FileTB.saveButton.setOnAction(e -> {
            if (SAVED == false) {
                TextInputDialog comfirmSave = new TextInputDialog();
                comfirmSave.setTitle("Save");
                comfirmSave.setHeaderText("Please Enter a Name to Save Your File Under");
                comfirmSave.setContentText("Save:");

                Optional<String> result = comfirmSave.showAndWait();
                if (result.isPresent()) {
                    ////TODO
                    String fileName = result.get();
                    try {
                        fController.handlerSaveAs(fileName);
                    } catch (IOException ex) {
                        //TODO EORRO
                    }
                }
            } else {
                try {
                    //If the File has already been save, just save and use old fileName
                    fController.handlerSave();
                } catch (IOException ex) {
                    Logger.getLogger(FileTbController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });

        FileTB.saveAsButton.setOnAction(e -> {
            TextInputDialog comfirmSave = new TextInputDialog();
            comfirmSave.setTitle("Save As");
            comfirmSave.setHeaderText("Please Enter a Name to Save Your File Under");
            comfirmSave.setContentText("Save As:");

            Optional<String> result = comfirmSave.showAndWait();
            if (result.isPresent()) {
                ////TODO
                String fileName = result.get();
                try {
                    fController.handlerSaveAs(fileName);
                } catch (IOException ex) {
                    //TODO EORRO
                }
            }

        });

        FileTB.loadButton.setOnAction(e -> {
            if (firstNew == true) {
                File fileToLoad = loadEport();
                fController.handlerLoad(fileToLoad);
            } else {
                if (EDITED.get() == true) {
                    if (promptToSave() == true) {
                        File fileToLoad = loadEport();
                        fController.handlerLoad(fileToLoad);
                    }
                    
                }else{
                      File fileToLoad = loadEport();
                     fController.handlerLoad(fileToLoad);
                }
              
            }

        });

        FileTB.exitButton.setOnAction(e -> {

            Alert comfirmExit = new Alert(AlertType.CONFIRMATION);
            comfirmExit.setTitle("Exit");
            comfirmExit.setHeaderText(null);
            comfirmExit.setContentText("Do you wish to exit?");

            Optional<ButtonType> result = comfirmExit.showAndWait();
            if (result.get() == ButtonType.OK) {
                primaryStage.close();
            } else {
                //exit prompt does nothing
            }
        });

        FileTB.exportButton.setOnAction(e -> {

            fController.handlerExport();
            
            
            Alert comfirmExport = new Alert(AlertType.INFORMATION);
            comfirmExport.setTitle("Exported");
            comfirmExport.setHeaderText(null);
            comfirmExport.setContentText("The ePortFolio has been Exported!");

            comfirmExport.showAndWait();
        });
    }

    public boolean promptToSave() {

        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Continue without Saving?");
        alert.setContentText("Do you wish to continue without Saving?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            return true;
        } else {
            return false;
        }
    }

    public void clearTabs() {
        pageTabs.getTabs().clear();

    }

    public void enableButtons(boolean enable) {

        //FILE TOOLBAR BUTTONS
        saveButton.setDisable(!enable);
        saveAsButton.setDisable(!enable);
        exportButton.setDisable(!enable);

        //SITE TOOLBAR BUTTONS
        addPageButt.setDisable(!enable);
        removePageButt.setDisable(!enable);
        selectPageButt.setDisable(!enable);

        //WORKSPACE TOOLBAR BUTTONS
        layoutButt.setDisable(!enable);
        colorButt.setDisable(!enable);
        fontButt.setDisable(!enable);
        textButt.setDisable(!enable);
        imgButt.setDisable(!enable);
        slideButt.setDisable(!enable);
        videoButt.setDisable(!enable);
        linkButt.setDisable(!enable);
        editButt.setDisable(enable);
        removeButt.setDisable(enable);
        footer.setDisable(!enable);

        //PAGE TITLE AND STUDENT NAME
        bannerTxtField.setDisable(!enable);
        pageTitleField.setDisable(!enable);
        bannerImage.setDisable(!enable);

    }

    public void makeNewEport() {

        Alert comfirmNew = new Alert(AlertType.INFORMATION);
        comfirmNew.setTitle("New ePortFolio");
        comfirmNew.setHeaderText(null);
        comfirmNew.setContentText("A new ePortFolio has been created");

        enableButtons(true);

        clearTabs();
        pageList.clear();
        pageData.clear();
        checkBoxList.clear();
        componentMap.clear();
        pageComp.clear();
        primaryStage.setTitle("ePortFolio Generator");
        SAVED = false;
        EDITED.set(true);
        saveFileName = "";

        //New Page ; Starter Page INDEX.HTML
        Tab index = new Tab();
        index.setText("index");
        index.setId("Index Tab");
        index.setClosable(false);
        Page indexData = new Page();

        pageData.put(index, indexData);

        ScrollPane sP = new ScrollPane();

        VBox components = new VBox();
        components.setSpacing(20);

        sP.setContent(components);
        index.setContent(sP);

        pageCount = 0;
        pageCount++;

        pageTabs.getTabs().add(index);
        pageList.add(index);
        pageComp.add(components);

        bannerTxtField.clear();

        comfirmNew.showAndWait();
        firstNew = false;
    }

    public File loadEport() {

        Alert comfirmLoad = new Alert(AlertType.INFORMATION);
        Stage fileStage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialDirectory(new File(SAVEPATH));
        File ePortFile = fileChooser.showOpenDialog(fileStage);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JSON", "*.json"));
        comfirmLoad.setTitle("Load");
        comfirmLoad.setHeaderText(null);

        if (ePortFile != null) {
            comfirmLoad.setContentText("EportFolio " + ePortFile.getName() + " has been loaded");
        } else {
            comfirmLoad.setHeaderText("File Error");
            comfirmLoad.setContentText("The EportFolio was not able to load or a File was not Selected");
        }

        comfirmLoad.showAndWait();
        firstNew = false;

        return ePortFile;
    }
    /**
        public void initSelectedTab() {

        modeTabPane.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Tab>() {
                    @Override
                    public void changed(ObservableValue<? extends Tab> ov, Tab t, Tab t1) {
                        if(t1 == editortab){
                            EnableButtons eB = new EnableButtons();
                            
                            eB.DisableButtons();
                        }else{
                            EnableButtons eB = new EnableButtons();
                        }
                    }
                });
    }*/
}
