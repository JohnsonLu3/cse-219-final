package Controller;

import static Controller.FileController.saveFileName;
import Page.Component;
import Page.Page;
import static Page.PageList.componentMap;
import static Page.PageList.pageComp;
import static Page.PageList.pageData;
import static Page.PageList.pageList;
import static View.WorkspaceToolBar_view.bannerText;
import static View.WorkspaceToolBar_view.bannerTxtField;
import static eportfoliogenerator.StartupConstantsEport.EXPORTPATH;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author JL
 */
public class ExportePort {

    String dirPath = EXPORTPATH + saveFileName;

    File ePortDir = new File(dirPath);
    File CssDir = new File(dirPath + "/css");
    File JsDir = new File(dirPath + "/js");
    File ImgDir = new File(dirPath + "/img");
    File VidDir = new File(dirPath + "/video");

    public void exportEport() {

        constructDir();
        constructPageObj();

    }

    public void constructDir() {

        if (!ePortDir.exists()) {
            ePortDir.mkdirs();
        }
        if (!CssDir.exists()) {
            CssDir.mkdir();
        }
        if (!JsDir.exists()) {
            JsDir.mkdir();
        }
        if (!ImgDir.exists()) {
            ImgDir.mkdir();
        }
        if (!VidDir.exists()) {
            VidDir.mkdir();
        }

        copyHTML();
        copyJS();
        File cssSrc = new File(EXPORTPATH + "base/css/");
        File cssDest = new File(dirPath + "/css/");

        File slideShowImgButt = new File(EXPORTPATH + "base/img/slideShow/");
        File imgButtDest = new File(dirPath + "/img/slideShow/");
        try {
            copyFolder(cssSrc, cssDest);
            copyFolder(slideShowImgButt, imgButtDest);
        } catch (IOException ex) {
            Logger.getLogger(ExportePort.class.getName()).log(Level.SEVERE, null, ex);
        }
        copyIMG();

        copyVID();
        copySlide();

    }

    public void copyHTML() {

        for (int i = 0; i < pageList.size(); i++) {

            Tab pageTab = pageList.get(i);
            File htmlTemplate = new File(EXPORTPATH + "base/index.html");
            File htmlDest = new File(dirPath + "/" + pageTab.getText() + ".html");

            try {
                if (!htmlDest.exists()) {
                    Files.copy(htmlTemplate.toPath(), htmlDest.toPath());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    public void copyJS() {

        File source = new File(EXPORTPATH + "base/js/generate.js");
        System.out.println("||SOURCE :: " + source.getAbsolutePath());
        File dest = new File(dirPath + "/js/" + source.getName());
        System.out.println("||DEST :: " + dest.getAbsolutePath());

        File slideSource = new File(EXPORTPATH + "base/js/slideShow.js");
        File slldeDest = new File(dirPath + "/js/" + slideSource.getName());

        try {
            if (!dest.exists()) {
                Files.copy(source.toPath(), dest.toPath());
                Files.copy(slideSource.toPath(), slldeDest.toPath());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void copyFolder(File src, File dest)
            throws IOException {

        if (src.isDirectory()) {

            //if directory not exists, create it
            if (!dest.exists()) {
                dest.mkdir();
                System.out.println("Directory copied from "
                        + src + "  to " + dest);
            }

            //list all the directory contents
            String files[] = src.list();

            for (String file : files) {
                //construct the src and dest file structure
                File srcFile = new File(src, file);
                File destFile = new File(dest, file);
                //recursive copy
                copyFolder(srcFile, destFile);
            }

        } else {
            //if file, then copy it
            //Use bytes stream to support all file types
            InputStream in = new FileInputStream(src);
            OutputStream out = new FileOutputStream(dest);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes 
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }

            in.close();
            out.close();
            System.out.println("File copied from " + src + " to " + dest);
        }
    }

    public void copySlide() {
        Iterator it = componentMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Component compToLook = new Component();
            compToLook = (Component) pair.getValue();

            if (compToLook.getComponentType().equals("SlideShow")) {

                File source = new File(compToLook.getSlideShowFilePath());
                System.out.println("||SOURCE :: " + source.getAbsolutePath());
                File dest = new File(dirPath + "/js/" + source.getName());
                System.out.println("||DEST :: " + dest.getAbsolutePath());

                try {
                    if (!dest.exists()) {
                        Files.copy(source.toPath(), dest.toPath());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void copyIMG() {
        
        
        for(int i=0; i<pageList.size(); i++){
        
            Page pageBanner = pageData.get(pageList.get(i));
            
            if(pageBanner.getBannerFile() != null){
                File bannerImage = pageBanner.getBannerFile();
                File bannerDest = new File(dirPath + "/img/" + bannerImage.getName());
                
                
                try {
                    if (!bannerDest.exists()) {
                        Files.copy(bannerImage.toPath(), bannerDest.toPath());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        
        Iterator it = componentMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Component compToLook = new Component();
            compToLook = (Component) pair.getValue();

            if (compToLook.getComponentType().equals("Image")) {

                File source = compToLook.getImgFile();
                System.out.println("||SOURCE :: " + source.getAbsolutePath());
                File dest = new File(dirPath + "/img/" + source.getName());
                System.out.println("||DEST :: " + dest.getAbsolutePath());

                try {
                    if (!dest.exists()) {
                        Files.copy(source.toPath(), dest.toPath());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void copyVID() {

        Iterator it = componentMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            Component compToLook = new Component();
            compToLook = (Component) pair.getValue();

            if (compToLook.getComponentType().equals("Video")) {

                File source = compToLook.getVidFile();
                File dest = new File(dirPath + "/video/" + source.getName());

                try {
                    if (!dest.exists()) {
                        Files.copy(source.toPath(), dest.toPath());
                    }
                } catch (IOException e) {
                    System.out.println("vid path does not exist");
                }

            }
        }

    }

    public void constructPageObj() {

        JSONObject pageObject = new JSONObject();
        initPageObjs(pageObject);
        
        File ePortJSON = new File(dirPath + "/js/" + "ePortFolio" + ".json");
        

        
        try {
            FileWriter writer = new FileWriter(ePortJSON);
            writer.write(pageObject.toJSONString());
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ExportePort.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    ////// CREATING THE JSON OBJECTS AND ARRAY FOR SAVING
    /**
     *
     * @param root
     */
    public void initPageObjs(JSONObject root) {
        System.out.println("PageList Size :: " + pageList.size() + " || PageData Size :: " + pageData.size());
        JSONArray pageArry = new JSONArray();

        for (int i = 0; i < pageList.size(); i++) {
            JSONObject pageObj = new JSONObject();
            Page page = pageData.get(pageList.get(i));
            pageObj.put("StudentName", bannerText);

            if(page.getBannerFile() != null){
                pageObj.put("BannerImage", page.getBannerFile().getName());
            }
            pageObj.put("Color", page.getColor());
            pageObj.put("Font", page.getFont());
            pageObj.put("Layout", page.getLayout());
            pageObj.put("FooterTxt", "<p>" + page.getFootereTxt() + "</p>");
            pageObj.put("Components", initComponentObjs(i));

            JSONObject pageData = new JSONObject();
            String pageName = pageList.get(i).getText();
            pageName = pageName.replaceAll("\\s+","");
            pageData.put( pageName+ ".html", pageObj);

            pageArry.add(pageData);

        }
        bannerTxtField.getText();
        root.put("ePortfolio", pageArry);

    }

    public JSONArray initComponentObjs(int pageIndex) {

        JSONArray ComponentArray = new JSONArray();

        //grabs each VBox from each page
        System.out.println(pageComp.size() + " || PageComp Size");
        VBox pageVBox = pageComp.get(pageIndex);

        for (int ii = 0; ii < pageVBox.getChildren().size(); ii++) {

            Node nodeOut = pageVBox.getChildren().get(ii);
            VBox component = null;

            if (nodeOut instanceof VBox) {
                component = (VBox) nodeOut;
            }

            Component comp = componentMap.get(component);
            String compType = comp.getComponentType();

            switch (compType) {

                case "Paragraph":
                    JSONObject ParaObj = initParagraphJSONObject(comp);
                    ComponentArray.add(ParaObj);
                    break;
                case "List":
                    JSONObject ListObj = initListJSONObject(comp);
                    ComponentArray.add(ListObj);
                    break;
                case "Header":
                    JSONObject HeadObj = initHeaderJSONObject(comp);
                    ComponentArray.add(HeadObj);
                    break;
                case "Image":
                    JSONObject ImgObj = initImageJSONObject(comp);
                    ComponentArray.add(ImgObj);
                    break;
                case "Video":
                    JSONObject VidObj = initVideoJSONObject(comp);
                    ComponentArray.add(VidObj);
                    break;
                case "SlideShow":
                    JSONObject SlideObj = initSlideShowJSONObject(comp);
                    ComponentArray.add(SlideObj);
                    break;

            }
        }

        return ComponentArray;
    }

    /**
     * initParagraphJSONObject Takes a paragraph Component and stores its data
     * into a JSON OBject.
     *
     * @param comp
     * @return
     */
    public JSONObject initParagraphJSONObject(Component comp) {

        JSONObject paraObj = new JSONObject();

        paraObj.put("Type", comp.getComponentType());
        paraObj.put("Text", comp.getText());
        paraObj.put("Font", comp.getFont());
        paraObj.put("Size", comp.getFontSize());

        return paraObj;
    }

    /**
     * initListJSONArray Takes a List Component and stores its data into a JSON
     * Object List items are stored in a JSON Array and then into the Object
     *
     * @param comp
     * @return
     */
    public JSONObject initListJSONObject(Component comp) {
        JSONObject listObj = new JSONObject();

        JSONArray listArray = new JSONArray();

        for (int i = 0; i < comp.getListItems().size(); i++) {

            listArray.add(comp.getListItems().get(i));
        }

        listObj.put("Type", comp.getComponentType());
        listObj.put("ListItems", listArray);

        return listObj;
    }

    /**
     * initHeader Takes a Header Component and stores its data into a JSON
     * Object
     *
     * @param comp
     * @return
     */
    public JSONObject initHeaderJSONObject(Component comp) {

        JSONObject headerObj = new JSONObject();
        headerObj.put("Type", comp.getComponentType());
        headerObj.put("Text", comp.getText());

        return headerObj;
    }

    /**
     * initImage Takes Image Component and stores its data into a JSON Object
     *
     * @param comp
     * @return
     */
    public JSONObject initImageJSONObject(Component comp) {
        JSONObject imgObj = new JSONObject();

        imgObj.put("Type", comp.getComponentType());
        imgObj.put("ImagePath", comp.getImgFile().getName());
        imgObj.put("Postition", comp.getImgPosition());
        imgObj.put("Height", comp.getImgHeight());
        imgObj.put("Width", comp.getImgWidth());
        imgObj.put("Caption", comp.getImgCaption());

        return imgObj;

    }

    /**
     * Takes video Component and stores its data into a JSON Object
     *
     * @param comp
     * @return
     */
    public JSONObject initVideoJSONObject(Component comp) {
        JSONObject vidObj = new JSONObject();

        vidObj.put("Type", comp.getComponentType());
        vidObj.put("VideoPath", comp.getVidFile().getName());
        vidObj.put("Height", comp.getVidHeight());
        vidObj.put("Width", comp.getVidWidth());
        vidObj.put("Caption", comp.getVidCaption());

        return vidObj;
    }

    /**
     * Takes SlideShow Component and stores its data into a JSON Object
     *
     * @param comp
     * @return
     */
    public JSONObject initSlideShowJSONObject(Component comp) {
        JSONObject slideObj = new JSONObject();

        slideObj.put("Type", comp.getComponentType());
        slideObj.put("SlideShowFile", comp.getSlideShowFile().getName());

        return slideObj;
    }
}
