package Controller;

import static View.FileToolbar_view.exportButton;
import static View.FileToolbar_view.saveAsButton;
import static View.SiteToolbar_view.addPageButt;
import static View.WorkspaceToolBar_view.bannerImage;
import static View.WorkspaceToolBar_view.bannerTxtField;
import static View.WorkspaceToolBar_view.colorButt;
import static View.WorkspaceToolBar_view.editButt;
import static View.WorkspaceToolBar_view.fontButt;
import static View.WorkspaceToolBar_view.footer;
import static View.WorkspaceToolBar_view.imgButt;
import static View.WorkspaceToolBar_view.layoutButt;
import static View.WorkspaceToolBar_view.linkButt;
import static View.WorkspaceToolBar_view.pageTitleField;
import static View.WorkspaceToolBar_view.removeButt;
import static View.WorkspaceToolBar_view.slideButt;
import static View.WorkspaceToolBar_view.textButt;
import static View.WorkspaceToolBar_view.videoButt;

/**
 *
 * @author JL
 */
public class EnableButtons {
    
    
    /**
     * Frankly I should have made this a method that takes a boolean and 
     * so that I woun't have to methods that do the same stuff depending on
     * it's boolean. Sadly due to my poor planning, its gonna stay like this.
     */
        public EnableButtons(){
    
        pageTitleField.setDisable(false);
        bannerTxtField.setDisable(false);
        bannerImage.setDisable(false);
        exportButton.setDisable(false);
        saveAsButton.setDisable(false);
        layoutButt.setDisable(false);
        colorButt.setDisable(false);
        fontButt.setDisable(false);
        textButt.setDisable(false);
        imgButt.setDisable(false);
        slideButt.setDisable(false);
        videoButt.setDisable(false);
        linkButt.setDisable(false);
        editButt.setDisable(false);
        addPageButt.setDisable(false);
        removeButt.setDisable(false);
        footer.setDisable(false);
    }
        
        public void DisableButtons(){
               
        pageTitleField.setDisable(true);
        bannerTxtField.setDisable(true);
        bannerImage.setDisable(true);
        layoutButt.setDisable(true);
        colorButt.setDisable(true);
        fontButt.setDisable(true);
        textButt.setDisable(true);
        imgButt.setDisable(true);
        slideButt.setDisable(true);
        videoButt.setDisable(true);
        linkButt.setDisable(true);
        editButt.setDisable(true);
        addPageButt.setDisable(true);
        removeButt.setDisable(true);
        footer.setDisable(true);
        
        }
}
