package Controller;

import static Controller.FileController.EDITED;
import static Controller.SiteTbController.selectedTabIndex;
import Page.Component;
import Page.Page;

import static Page.PageList.componentMap;
import static Page.PageList.pageComp;
import static Page.PageList.pageData;
import static Page.PageList.pageList;
import static View.Eport_view.mainPane;
import static View.SiteEditorView.pageTabs;
import View.WorkspaceToolBar_view;
import View.ToolPrompts.ColorPrompt;
import View.ToolPrompts.FontPrompt;
import View.ToolPrompts.FooterPrompt;
import View.ToolPrompts.HyperLinkPrompt;
import View.ToolPrompts.ImagePrompt;
import View.ToolPrompts.LayoutPrompt;
import View.ToolPrompts.TextEditPrompt;
import View.ToolPrompts.VideoPrompt;
import static View.WorkspaceToolBar_view.bannerFileName;
import static View.WorkspaceToolBar_view.editButt;
import static View.WorkspaceToolBar_view.removeButt;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENT;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENTTYPE;
import static eportfoliogenerator.StartupConstantsEport.CSS_SELECTEDCOMP;
import java.io.File;
import java.util.ArrayList;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import ssm.SlideShowMaker;

/**
 *
 * @author JL
 */
public class WorkspaceToolBar_controller {

    WorkspaceToolBar_view workspaceTB;
    private String layoutChoice = "Article";
    private String colorChoice = "Albany";
    private String fontChoice = "Economica";
    private String footerText = "";

    public static VBox selectedComponent = null;

    public WorkspaceToolBar_controller(WorkspaceToolBar_view WSTB) {

        workspaceTB = WSTB;
        initHandlers();
    }

    private void initHandlers() {

        workspaceTB.bannerImage.setOnAction(e -> {
            Stage fileStage = new Stage();

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Banner Image File");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));

            File selectedFile = fileChooser.showOpenDialog(fileStage);

            if (selectedFile != null) {
                bannerFileName.setText(selectedFile.getName());
                Tab selectedPageTab = pageTabs.getSelectionModel().getSelectedItem();
                Page tabData = pageData.get(selectedPageTab);
                tabData.setBannerImage(selectedFile);
                EDITED.set(true);
            }
        });

        workspaceTB.layoutButt.setOnAction(e -> {

            Tab selectedPageTab = pageTabs.getSelectionModel().getSelectedItem();
            Page tabData = pageData.get(selectedPageTab);

            LayoutPrompt LOP = new LayoutPrompt();
            layoutChoice = LOP.layoutPicker(tabData.getLayout());

            tabData.setLayout(layoutChoice);
            EDITED.set(true);
            System.out.println("Current Layout : " + layoutChoice);

        });

        workspaceTB.colorButt.setOnAction(e -> {

            Tab selectedPageTab = pageTabs.getSelectionModel().getSelectedItem();
            Page tabData = pageData.get(selectedPageTab);

            ColorPrompt CSP = new ColorPrompt();
            colorChoice = CSP.colorPicker(tabData.getColor());

            tabData.setColor(colorChoice);
            EDITED.set(true);
            System.out.println("Current Color Scheme : " + colorChoice);

        });

        workspaceTB.fontButt.setOnAction(e -> {

            Tab selectedPageTab = pageTabs.getSelectionModel().getSelectedItem();
            Page tabData = pageData.get(selectedPageTab);

            FontPrompt FCP = new FontPrompt();
            fontChoice = FCP.fontPicker(tabData.getFont());

            tabData.setFont(fontChoice);
            EDITED.set(true);
            System.out.println("Current Font : " + fontChoice);

        });

        workspaceTB.textButt.setOnAction(e -> {
            textPrompt();

        });

        workspaceTB.videoButt.setOnAction(e -> {
            videoPrompt();

        });

        workspaceTB.imgButt.setOnAction(e -> {
            imgPrompt();
        });

        workspaceTB.slideButt.setOnAction(e -> {
            slideShowPrompt();
        });

        workspaceTB.linkButt.setOnAction(e -> {
            linkPrompt();
        });

        workspaceTB.editButt.setOnAction(e -> {
            editPrompt();
        });

        workspaceTB.removeButt.setOnAction(e -> {
            Alert comfirmRemove = new Alert(AlertType.CONFIRMATION);
            comfirmRemove.setTitle("Confirm Remove");
            comfirmRemove.setHeaderText("Remove Selected Component(s)?");
            comfirmRemove.setContentText("Do you wish to remove the following selected component(s)?");

            Optional<ButtonType> result = comfirmRemove.showAndWait();
            if (result.get() == ButtonType.OK) {
                try {
                    VBox pageComponents = pageComp.get(selectedTabIndex);
                    pageComponents.getChildren().remove(selectedComponent);
                    System.out.println("MAP SIZE ::" + componentMap.size());
                    System.out.println("MAP CONTAINS SELECTED :: " + componentMap.containsKey(selectedComponent));

                    componentMap.remove(selectedComponent);

                    selectedComponent = null;
                    removeButt.setDisable(true);
                    editButt.setDisable(true);
                    EDITED.set(true);

                } catch (NullPointerException NE) {
                    System.out.println("No selected Component to remove on current Page");
                    Alert deleteERROR = new Alert(AlertType.INFORMATION);
                    deleteERROR.setTitle("ERROR");
                    deleteERROR.setContentText("No selected Component to remove on current Page");

                    deleteERROR.showAndWait();
                }
            } else {
                // ... user chose CANCEL or closed the dialog
            }
        });

        workspaceTB.footer.setOnAction(e -> {
            footerPrompt();
        });

    }

    //Prompt Init
    public void textPrompt() {
        TextEditPrompt TEP = new TextEditPrompt();

        TEP.textEditor();

        TEP.okay.setOnAction(ee -> {

            TEP.textEditorDialog.closePopUp();

            String type = TEP.getTextType();
            VBox textComp = new VBox();
            Label textType = new Label(type);
            textType.getStyleClass().add(CSS_COMPONENTTYPE);

            textComp.getChildren().add(textType);
            textComp.getStyleClass().add(CSS_COMPONENT);
            textComp.setPrefWidth(1000);

            VBox comp = pageComp.get(selectedTabIndex);
            initSelectHandler(textComp);

            Component paraComp = new Component();
            comp.getChildren().add(textComp);

            if (type.equals("List")) {
                TEP.setListItems();
                ArrayList<String> listItemsText = TEP.getListItems();
                paraComp.setComponentType(type);
                paraComp.setListItems(listItemsText);

            } else {

                if (type.equals("Paragraph")) {
                    paraComp.setComponentType(type);
                    paraComp.setText(TEP.getTextResult());
                    paraComp.setFont(TEP.getTextFont());
                    paraComp.setFontSize(TEP.getTextSize());
                } else {
                    if (type.equals("Header")) {
                        paraComp.setComponentType(type);
                        paraComp.setText(TEP.getTextResult());
                    }
                }
            }

            //Add Data to their List
            updateMap(textComp, paraComp);
            EDITED.set(true);
            System.out.println(TEP.getTextResult());

            //QUICKLY SWITCHS BETWEEN TABS TO GET 
            if (pageTabs.getTabs().size() == 1) {
                Tab tempTab = new Tab();
                pageTabs.getTabs().add(tempTab);
                pageTabs.getSelectionModel().selectNext();
                pageTabs.getSelectionModel().selectPrevious();
                pageTabs.getTabs().remove(tempTab);
            }
            if (pageTabs.getSelectionModel().isSelected(pageTabs.getTabs().size() - 1)) {

                pageTabs.getSelectionModel().selectPrevious();
                pageTabs.getSelectionModel().selectNext();
            } else {
                pageTabs.getSelectionModel().selectNext();
                pageTabs.getSelectionModel().selectPrevious();

            }

        });

        TEP.cancel.setOnAction(eee -> {
            TEP.textEditorDialog.closePopUp();
        });
    }

    //PARAGRAPH Text Component
    public void paragraphPrompt(String text, String font, String fontSize, Component eComp) {
        TextEditPrompt TEP = new TextEditPrompt();

        TEP.Paragraph.setSelected(true);
        TEP.textArea.setText(text);
        TEP.fontFamily.setValue(font);
        TEP.sizeInput.setText(fontSize);
        TEP.List.setDisable(true);
        TEP.Header.setDisable(true);

        TEP.okay.setOnAction(e -> {
            eComp.setText(TEP.getTextResult());
            eComp.setFont(TEP.getTextFont());
            eComp.setFontSize(TEP.getTextSize());
            TEP.textEditorDialog.closePopUp();
            EDITED.set(true);
        });

        TEP.cancel.setOnAction(e -> {

            TEP.textEditorDialog.closePopUp();
        });

        TEP.textEditor();
    }

    //HEADER EDIT PROMPT
    public void headerPrompt(String text, Component eComp) {
        TextEditPrompt TEP = new TextEditPrompt();

        TEP.Header.setSelected(true);
        TEP.Paragraph.setDisable(true);
        TEP.List.setDisable(true);
        TEP.textEditor();
        System.out.println("header Text to Edit :: " + text);
        TEP.header.setText(text);

        TEP.okay.setOnAction(e -> {
            System.out.println("**" + TEP.getTextResult());
            eComp.setText(TEP.getTextResult());
            TEP.textEditorDialog.closePopUp();
            System.out.println("****" + eComp.getText());
            EDITED.set(true);
        });

        TEP.cancel.setOnAction(e -> {

            TEP.textEditorDialog.closePopUp();
        });

    }

    public void listPrompt(ArrayList<String> lItems, Component eComp) {
        TextEditPrompt TEP = new TextEditPrompt();
        //DO THIS
        TEP.List.setSelected(true);
        TEP.Header.setDisable(true);
        TEP.Paragraph.setDisable(true);
        //show list
        TEP.textEditor();
        //set up the list to edit
        TEP.fillList(eComp.getListItems());

        TEP.okay.setOnAction(e -> {
            System.out.println("**" + TEP.getTextResult());
            eComp.setText(TEP.getTextResult());
            TEP.setListItems();
            ArrayList<String> listItemsText = TEP.getListItems();
            eComp.setListItems(listItemsText);
            TEP.textEditorDialog.closePopUp();
            EDITED.set(true);
        });

        TEP.cancel.setOnAction(e -> {

            TEP.textEditorDialog.closePopUp();
        });
    }

    public void imgPrompt() {

        ImagePrompt IEP = new ImagePrompt();

        IEP.imageEditor();

        IEP.okay.setOnAction(ee -> {
            if (IEP.getFile() == null) {
                Alert fileNotFound = new Alert(AlertType.WARNING);
                fileNotFound.setTitle("No File Was Selected");
                fileNotFound.setHeaderText("Missing Image File");
                fileNotFound.setContentText("You have not selected a Image File to add as a Component");

                fileNotFound.showAndWait();

            } else {

                IEP.imageDialog.closePopUp();

                VBox textComp = new VBox();
                Label textType = new Label("Image");
                textType.getStyleClass().add(CSS_COMPONENTTYPE);

                textComp.getChildren().add(textType);
                textComp.getStyleClass().add(CSS_COMPONENT);
                textComp.setPrefWidth(1000);

                VBox comp = pageComp.get(selectedTabIndex);
                initSelectHandler(textComp);
                //add component to map for tracking
                Component paraComp = new Component();
                paraComp.setComponentType(IEP.getType());
                paraComp.setImgHeight(IEP.getImageHeight());
                paraComp.setImgWidth(IEP.getImageWidth());
                paraComp.setImgFile(IEP.getFile());
                paraComp.setImgPosition(IEP.getPosition());
                paraComp.setImgCaption(IEP.getImageCaption());

                //Add Data to their List
                updateMap(textComp, paraComp);
                EDITED.set(true);

                comp.getChildren().add(textComp);
                if (pageTabs.getTabs().size() == 1) {
                    Tab tempTab = new Tab();
                    pageTabs.getTabs().add(tempTab);
                    pageTabs.getSelectionModel().selectNext();
                    pageTabs.getSelectionModel().selectPrevious();
                    pageTabs.getTabs().remove(tempTab);
                }
                if (pageTabs.getSelectionModel().isSelected(pageTabs.getTabs().size() - 1)) {
                    pageTabs.getSelectionModel().selectPrevious();
                    pageTabs.getSelectionModel().selectNext();
                } else {
                    pageTabs.getSelectionModel().selectNext();
                    pageTabs.getSelectionModel().selectPrevious();
                }
            }
        });

        IEP.cancel.setOnAction(eee -> {
            IEP.imageDialog.closePopUp();
        });

    }

    public void imgPrompt(String position, String height, String width, String caption, Component eComp) {

        ImagePrompt IEP = new ImagePrompt();

        IEP.imageEditor();

        //Fill in DATA
        IEP.setSelectedPosition(position);
        IEP.fillHeight(height);
        IEP.fillWidth(width);
        IEP.fillCaption(caption);
        IEP.fillFileLabel(eComp.getImgFile().getName());
        IEP.setFile(eComp.getImgFile());

        IEP.okay.setOnAction(e -> {

            eComp.setImgHeight(IEP.getImageHeight());
            eComp.setImgWidth(IEP.getImageWidth());
            eComp.setImgPosition(IEP.getPosition());
            eComp.setImgCaption(IEP.getImageCaption());
            eComp.setImgFile(IEP.getFile());
            IEP.imageDialog.closePopUp();
            EDITED.set(true);

        });

        IEP.cancel.setOnAction(e -> {

            IEP.imageDialog.closePopUp();
        });
    }

    public void videoPrompt() {

        VideoPrompt VEP = new VideoPrompt();

        VEP.videoEditor();

        VEP.okay.setOnAction(ee -> {

            if (VEP.getFile() == null) {
                Alert fileNotFound = new Alert(AlertType.WARNING);
                fileNotFound.setTitle("No File Was Selected");
                fileNotFound.setHeaderText("Missing Video File");
                fileNotFound.setContentText("You have not selected a Video File to add as a Component");

                fileNotFound.showAndWait();

            } else {
                VBox textComp = new VBox();
                Label textType = new Label("Video");
                textType.getStyleClass().add(CSS_COMPONENTTYPE);

                textComp.getChildren().add(textType);
                textComp.getStyleClass().add(CSS_COMPONENT);
                textComp.setPrefWidth(1000);

                VBox comp = pageComp.get(selectedTabIndex);
                initSelectHandler(textComp);

                //add component to map for tracking
                Component paraComp = new Component();
                paraComp.setComponentType(VEP.getType());
                paraComp.setVidHeight(VEP.getVideoHeight());
                paraComp.setVidWidth(VEP.getVideoWidth());
                paraComp.setVidFile(VEP.getFile());
                paraComp.setVidCaption(VEP.getVideoCaption());

                //Add Data to their List
                updateMap(textComp, paraComp);
                EDITED.set(true);

                comp.getChildren().add(textComp);
                if (pageTabs.getTabs().size() == 1) {
                    Tab tempTab = new Tab();
                    pageTabs.getTabs().add(tempTab);
                    pageTabs.getSelectionModel().selectNext();
                    pageTabs.getSelectionModel().selectPrevious();
                    pageTabs.getTabs().remove(tempTab);
                }
                if (pageTabs.getSelectionModel().isSelected(pageTabs.getTabs().size() - 1)) {
                    pageTabs.getSelectionModel().selectPrevious();
                    pageTabs.getSelectionModel().selectNext();
                } else {
                    pageTabs.getSelectionModel().selectNext();
                    pageTabs.getSelectionModel().selectPrevious();
                }

                VEP.videoDialog.closePopUp();
            }
        });

        VEP.cancel.setOnAction(eee -> {
            VEP.videoDialog.closePopUp();
        });

    }

    public void videoPrompt(String h, String w, String cap, Component eComp) {
        VideoPrompt VEP = new VideoPrompt();

        VEP.videoEditor();

        //filling the textfields
        VEP.fillVideoHeight(h);
        VEP.fillVideoWidth(w);
        VEP.fillVideoCaption(cap);
        VEP.fillFileLabel(eComp.getVidFile().getName());
        VEP.setFile(eComp.getVidFile());

        VEP.okay.setOnAction(e -> {

            eComp.setVidHeight(VEP.getVideoHeight());
            eComp.setVidWidth(VEP.getVideoWidth());
            eComp.setVidCaption(VEP.getVideoCaption());
            eComp.setVidFile(VEP.getFile());
            VEP.videoDialog.closePopUp();
            EDITED.set(true);

        });

        VEP.cancel.setOnAction(e -> {

            VEP.videoDialog.closePopUp();
        });
    }

    public void editPrompt() {
        if (selectedComponent != null) {

            Component editComp = componentMap.get(selectedComponent);

            String compType = editComp.getComponentType();
            System.out.println("Component Type :: " + compType);

            switch (compType) {

                case "Paragraph":
                    paragraphPrompt(editComp.getText(), editComp.getFont(), editComp.getFontSize(), editComp);
                    break;
                case "Header":
                    headerPrompt(editComp.getText(), editComp);
                    break;
                case "List":
                    listPrompt(editComp.getListItems(), editComp);
                    break;

                case "Image":
                    imgPrompt(editComp.getImgPosition(), editComp.getImgHeight(), editComp.getImgWidth(), editComp.getImgCaption(), editComp);
                    break;
                case "Video":
                    videoPrompt(editComp.getVidHeight(), editComp.getVidWidth(), editComp.getVidCaption(), editComp);
                    break;
                case "SlideShow":
                    slideShowPrompt(editComp.getSlideShowFilePath(), editComp, selectedComponent);
                    break;
                default:
                    break;
            }
        }
    }

    public void footerPrompt() {

        Tab currentTab = pageTabs.getSelectionModel().getSelectedItem();
        Page currentPage = pageData.get(currentTab);

        FooterPrompt FEP = new FooterPrompt();

        FEP.footerEditor(currentPage.getFootereTxt());

        FEP.okay.setOnAction(e -> {

            footerText = FEP.getFooterText();
            currentPage.setFooterText(footerText);
            FEP.footerEditorDialog.closePopUp();
            EDITED.set(true);
        });

        FEP.cancel.setOnAction(e -> {
            //FEP.comfrimExit();
        });

        System.out.println(FEP.getFooterText());
    }

    public void linkPrompt() {
        HyperLinkPrompt HLEP = new HyperLinkPrompt();
        System.out.print("Jello Link");
        HLEP.linkEditor();

        HLEP.add.setOnAction(e -> {
            HLEP.linkEditor.closePopUp();
            EDITED.set(true);
        });

        HLEP.cancel.setOnAction(e -> {
            HLEP.linkEditor.closePopUp();
        });
    }

    /**
     * Adding a SlideSHow Component
     */
    public void slideShowPrompt() {
        SlideShowMaker SSM = new SlideShowMaker();
        Stage slideShowEditor = new Stage();
        try {
            SSM.start(slideShowEditor);

            slideShowEditor.setOnCloseRequest(e -> {

                String slideShowFilePath = SSM.getFile();
                File slideShowFile = new File(SSM.getFile());

                System.out.println(slideShowFilePath);
                if (SSM.getFileSaved() == true) {

                    SSM.clearFile();

                    if (slideShowFilePath != null || !slideShowFilePath.equals("")) {
                        VBox slideComp = new VBox();
                        Label compType = new Label("SlideShow");
                        String fileName = slideShowFilePath.substring(20, slideShowFilePath.length() - 5);

                        Label pathLabel = new Label(fileName);
                        slideComp.getChildren().add(compType);
                        slideComp.getChildren().add(pathLabel);

                        VBox comp = pageComp.get(selectedTabIndex);
                        comp.setPrefWidth(1000);
                        compType.getStyleClass().add(CSS_COMPONENTTYPE);

                        initSelectHandler(slideComp);

                        Component slideComponent = new Component();
                        slideComponent.setSlideShowFilePath(slideShowFilePath);
                        slideComponent.setSlideShowFile(slideShowFile);
                        slideComponent.setComponentType("SlideShow");

                        updateMap(slideComp, slideComponent);
                        EDITED.set(true);

                        slideComp.getStyleClass().add(CSS_COMPONENT);
                        comp.getChildren().add(slideComp);

                    }
                }
                if (pageTabs.getTabs().size() == 1) {
                    Tab tempTab = new Tab();
                    pageTabs.getTabs().add(tempTab);
                    pageTabs.getSelectionModel().selectNext();
                    pageTabs.getSelectionModel().selectPrevious();
                    pageTabs.getTabs().remove(tempTab);
                }
                if (pageTabs.getSelectionModel().isSelected(pageTabs.getTabs().size() - 1)) {
                    pageTabs.getSelectionModel().selectPrevious();
                    pageTabs.getSelectionModel().selectNext();
                } else {
                    pageTabs.getSelectionModel().selectNext();
                    pageTabs.getSelectionModel().selectPrevious();
                }
            });

        } catch (Exception ex) {
            Logger.getLogger(WorkspaceToolBar_controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Edit SlideShow Component
     *
     * @param filePath
     * @param eComp
     * @param selectedVbox
     */
    public void slideShowPrompt(String filePath, Component eComp, VBox selectedVbox) {
        SlideShowMaker SSM = new SlideShowMaker();
        Stage slideShowEditor = new Stage();
        try {
            SSM.start(slideShowEditor);
            SSM.loadSlideShow(filePath);

            SSM.setFile(filePath);

        } catch (Exception ex) {
            //TODO ERROR MESSAGE
        }

        slideShowEditor.setOnCloseRequest(e -> {

            String slideShowFilePath = SSM.getFile();
            File slideShowFile = new File(SSM.getFile());

            if (slideShowFilePath != null) {

                eComp.setSlideShowFilePath(slideShowFilePath);
                eComp.setSlideShowFile(slideShowFile);
                if (pageTabs.getTabs().size() == 1) {
                    Tab tempTab = new Tab();
                    pageTabs.getTabs().add(tempTab);
                    pageTabs.getSelectionModel().selectNext();
                    pageTabs.getSelectionModel().selectPrevious();
                    pageTabs.getTabs().remove(tempTab);
                }
                if (pageTabs.getSelectionModel().isSelected(pageTabs.getTabs().size() - 1)) {
                    pageTabs.getSelectionModel().selectPrevious();
                    pageTabs.getSelectionModel().selectNext();
                } else {
                    pageTabs.getSelectionModel().selectNext();
                    pageTabs.getSelectionModel().selectPrevious();
                }
                SSM.clearFile();
                String fileName = slideShowFilePath.substring(20, slideShowFilePath.length() - 5);
                Label pathLabel = new Label(fileName);
                EDITED.set(true);
                if(selectedVbox.getChildren().size() > 1){
                    selectedVbox.getChildren().remove(1);
                }
                selectedVbox.getChildren().add(pathLabel);
            }

        });

    }

    public void initSelectHandler(VBox componentBox) {

        componentBox.setOnMouseClicked(e -> {
            System.out.println("A component is SELECTED");

            //SELECT COMPONENT IF NONE
            if (selectedComponent == null) {
                selectedComponent = componentBox;
                componentBox.getStyleClass().remove(CSS_COMPONENT);
                componentBox.getStyleClass().add(CSS_SELECTEDCOMP);
                removeButt.setDisable(false);
                editButt.setDisable(false);

            }

            if (componentBox != selectedComponent) {
                //DESELECT COMPONENT and SELECT NEW
                selectedComponent.getStyleClass().remove(CSS_SELECTEDCOMP);
                selectedComponent.getStyleClass().add(CSS_COMPONENT);
                selectedComponent = componentBox;
                componentBox.getStyleClass().remove(CSS_COMPONENT);
                componentBox.getStyleClass().add(CSS_SELECTEDCOMP);
            }

        });
    }

    public void updateMap(VBox Comp, Component CompData) {
        componentMap.put(Comp, CompData);
    }

}
