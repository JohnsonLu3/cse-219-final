package Controller;

import Page.Component;
import Page.Page;
import static Page.PageList.checkBoxList;
import static Page.PageList.componentMap;
import static Page.PageList.pageComp;
import static Page.PageList.pageData;
import static Page.PageList.pageList;
import static View.Eport_view.primaryStage;
import static View.FileToolbar_view.saveButton;
import static View.SiteEditorView.pageTabs;
import static View.SiteToolbar_view.STUDENTNAME;
import static View.WorkspaceToolBar_view.bannerText;
import static View.WorkspaceToolBar_view.bannerTxtField;
import static View.WorkspaceToolBar_view.pageTitleField;
import static eportfoliogenerator.StartupConstantsEport.SAVEPATH;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.VBox;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author JL
 */
public class FileController {

    public static boolean SAVED = false;
    public static String saveFileName = "";
    public static BooleanProperty EDITED = new SimpleBooleanProperty(false);

    public FileController() {
        initEditedListener();
    }

    public void handlerSaveAs(String fName) throws IOException {

        String fileName = fName;
        String jsonFilePath = SAVEPATH + fName + ".json";
        saveFileName = fName;

        System.out.println(jsonFilePath);

        JSONObject root = new JSONObject();

        initPageObjs(root);

        File saveFile = new File(jsonFilePath);

        FileWriter writer = new FileWriter(saveFile);
        writer.write(root.toJSONString());
        writer.flush();
        writer.close();
        primaryStage.setTitle(fileName);
        SAVED = true;
        EDITED.set(false);
    }

    public void handlerSave() throws IOException {

        JSONObject root = new JSONObject();

        initPageObjs(root);

        File saveFile = new File(SAVEPATH + saveFileName + ".json");

        FileWriter writer = new FileWriter(saveFile);
        writer.write(root.toJSONString());
        writer.flush();
        writer.close();
        primaryStage.setTitle(saveFileName);
        SAVED = true;
        EDITED.set(false);
    }

    /**
     * Load in a EportFolio using JSON
     *
     * @param fileToLoad
     */
    public void handlerLoad(File fileToLoad) {

        try {
            String fileToLoadName = fileToLoad.getName().substring(0 , (fileToLoad.getName().length()-5));
            pageTabs.getTabs().clear();
            pageList.clear();
            pageData.clear();
            checkBoxList.clear();
            componentMap.clear();
            pageComp.clear();
            primaryStage.setTitle(fileToLoadName);
            SAVED = true;
            EDITED.set(false);
            saveFileName = fileToLoadName;
            loadEportFolioController loadEport = new loadEportFolioController(fileToLoad);
            
            EnableButtons eB = new EnableButtons();
        } catch (FileNotFoundException ex) {
            System.out.println("SAVE FILE TO LOAD WAS NOT SELECTED");
        } catch( NullPointerException nu){
             System.out.println("SAVE FILE TO LOAD WAS NOT SELECTED");
        }

        
    }
    
    /**
     * Export ePortFolio using JSON
     */
    public void handlerExport(){
        
        ExportePort export = new ExportePort();
        export.exportEport();
        render rdr = new render();
        rdr.renderEport();
    }

    ////// CREATING THE JSON OBJECTS AND ARRAY FOR SAVING
    /**
     *
     * @param root
     */
    public void initPageObjs(JSONObject root) {
        System.out.println("PageList Size :: " + pageList.size() + " || PageData Size :: " + pageData.size());
        JSONArray pageArry = new JSONArray();

        for (int i = 0; i < pageList.size(); i++) {
            JSONObject pageObj = new JSONObject();
            Page page = pageData.get(pageList.get(i));
            pageObj.put("StudentName", bannerText);
            pageObj.put("BannerImage", page.getBannerImage());
            pageObj.put("Color", page.getColor());
            pageObj.put("Font", page.getFont());
            pageObj.put("Layout", page.getLayout());
            pageObj.put("FooterTxt", page.getFootereTxt());
            pageObj.put("Components", initComponentObjs(i));

            JSONObject pageData = new JSONObject();
            pageData.put(pageList.get(i).getText(), pageObj);

            pageArry.add(pageData);

        }
        bannerTxtField.getText();
        root.put(saveFileName, pageArry);

    }

    public JSONArray initComponentObjs(int pageIndex) {

        JSONArray ComponentArray = new JSONArray();

        //grabs each VBox from each page
        System.out.println(pageComp.size() + " || PageComp Size");
        VBox pageVBox = pageComp.get(pageIndex);

        for (int ii = 0; ii < pageVBox.getChildren().size(); ii++) {

            Node nodeOut = pageVBox.getChildren().get(ii);
            VBox component = null;

            if (nodeOut instanceof VBox) {
                component = (VBox) nodeOut;
            }

            Component comp = componentMap.get(component);
            String compType = comp.getComponentType();

            switch (compType) {

                case "Paragraph":
                    JSONObject ParaObj = initParagraphJSONObject(comp);
                    ComponentArray.add(ParaObj);
                    break;
                case "List":
                    JSONObject ListObj = initListJSONObject(comp);
                    ComponentArray.add(ListObj);
                    break;
                case "Header":
                    JSONObject HeadObj = initHeaderJSONObject(comp);
                    ComponentArray.add(HeadObj);
                    break;
                case "Image":
                    JSONObject ImgObj = initImageJSONObject(comp);
                    ComponentArray.add(ImgObj);
                    break;
                case "Video":
                    JSONObject VidObj = initVideoJSONObject(comp);
                    ComponentArray.add(VidObj);
                    break;
                case "SlideShow":
                    JSONObject SlideObj = initSlideShowJSONObject(comp);
                    ComponentArray.add(SlideObj);
                    break;

            }
        }

        return ComponentArray;
    }

    /**
     * initParagraphJSONObject Takes a paragraph Component and stores its data
     * into a JSON OBject.
     *
     * @param comp
     * @return
     */
    public JSONObject initParagraphJSONObject(Component comp) {

        JSONObject paraObj = new JSONObject();

        paraObj.put("Type", comp.getComponentType());
        paraObj.put("Text", comp.getText());
        paraObj.put("Font", comp.getFont());
        paraObj.put("Size", comp.getFontSize());

        return paraObj;
    }

    /**
     * initListJSONArray Takes a List Component and stores its data into a JSON
     * Object List items are stored in a JSON Array and then into the Object
     *
     * @param comp
     * @return
     */
    public JSONObject initListJSONObject(Component comp) {
        JSONObject listObj = new JSONObject();

        JSONArray listArray = new JSONArray();

        for (int i = 0; i < comp.getListItems().size(); i++) {

            listArray.add(comp.getListItems().get(i));
        }

        listObj.put("Type", comp.getComponentType());
        listObj.put("ListItems", listArray);

        return listObj;
    }

    /**
     * initHeader Takes a Header Component and stores its data into a JSON
     * Object
     *
     * @param comp
     * @return
     */
    public JSONObject initHeaderJSONObject(Component comp) {

        JSONObject headerObj = new JSONObject();
        headerObj.put("Type", comp.getComponentType());
        headerObj.put("Text", comp.getText());

        return headerObj;
    }

    /**
     * initImage Takes Image Component and stores its data into a JSON Object
     *
     * @param comp
     * @return
     */
    public JSONObject initImageJSONObject(Component comp) {
        JSONObject imgObj = new JSONObject();

        imgObj.put("Type", comp.getComponentType());
        imgObj.put("ImagePath", comp.getImgFile().getPath());
        imgObj.put("Postition", comp.getImgPosition());
        imgObj.put("Height", comp.getImgHeight());
        imgObj.put("Width", comp.getImgWidth());
        imgObj.put("Caption", comp.getImgCaption());

        return imgObj;

    }

    /**
     * Takes video Component and stores its data into a JSON Object
     *
     * @param comp
     * @return
     */
    public JSONObject initVideoJSONObject(Component comp) {
        JSONObject vidObj = new JSONObject();

        vidObj.put("Type", comp.getComponentType());
        vidObj.put("VideoPath", comp.getVidFile().getPath());
        vidObj.put("Height", comp.getVidHeight());
        vidObj.put("Width", comp.getVidWidth());
        vidObj.put("Caption", comp.getVidCaption());

        return vidObj;
    }

    /**
     * Takes SlideShow Component and stores its data into a JSON Object
     *
     * @param comp
     * @return
     */
    public JSONObject initSlideShowJSONObject(Component comp) {
        JSONObject slideObj = new JSONObject();

        slideObj.put("Type", comp.getComponentType());
        slideObj.put("SlideShowFile", comp.getSlideShowFilePath());

        return slideObj;
    }

    ///////
    /////////////////////
    //////END OF JSON OBJECT CREATION FOR SAVING
    /**
     * Boolean change listener
     */
    public void initEditedListener() {

        EDITED.addListener(new ChangeListener<Boolean>() {

            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                System.out.println("changed " + oldValue + "->" + newValue);

                if (newValue == true) {

                    saveButton.setDisable(false);
                } else {

                    saveButton.setDisable(true);
                }

            }
        });
    }

}
