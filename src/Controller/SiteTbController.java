package Controller;

import static Controller.FileController.EDITED;
import static Controller.WorkspaceToolBar_controller.selectedComponent;
import Page.Page;
import static Page.PageList.checkBoxList;
import static Page.PageList.pageComp;
import static Page.PageList.pageData;
import static Page.PageList.pageList;
import static View.SiteEditorView.pageTabs;
import View.SiteToolbar_view;
import static View.SiteToolbar_view.removePageButt;
import static View.WorkspaceToolBar_view.bannerFileName;
import static View.WorkspaceToolBar_view.bannerText;
import static View.WorkspaceToolBar_view.bannerTxtField;
import static View.WorkspaceToolBar_view.editButt;
import static View.WorkspaceToolBar_view.pageTitleField;
import static View.WorkspaceToolBar_view.removeButt;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENT;
import static eportfoliogenerator.StartupConstantsEport.CSS_SELECTEDCOMP;
import java.util.Optional;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class SiteTbController {

    SiteToolbar_view siteTB;
    public static int pageCount = 0;
    public static Tab selectedTab;
    public static int selectedTabIndex;
    public static Boolean multiSelect = false;

    public SiteTbController(SiteToolbar_view STBV) {

        siteTB = STBV;
        initSiteHandlers();
        initSelectedTab();
    }

    private void initSiteHandlers() {

        /**
         * Add a new Page Button Handler
         */
        siteTB.addPageButt.setOnAction(e -> {
            Alert comfirmSelect = new Alert(Alert.AlertType.INFORMATION);
            comfirmSelect.setTitle("New Page");
            comfirmSelect.setHeaderText(null);
            comfirmSelect.setContentText("New Page has been added.");

            //New Page
            Tab page = initTab();
            Label selectPageLabel = new Label("Select Page : ");
            Page newPageData = new Page();
            pageData.put(page, newPageData);
            /*
            //Check Boxes for multi-Selection
            CheckBox selectPageBox = new CheckBox();
            selectPageBox.setDisable(true);
            checkBoxList.add(selectPageBox);
            //CheckBoxes
            HBox selectPageHBox = new HBox();
            selectPageHBox.setSpacing(15);

            selectPageHBox.getChildren().add(selectPageLabel);
            selectPageHBox.getChildren().add(selectPageBox);
            */
            ScrollPane sP = new ScrollPane();

            VBox components = new VBox();
            components.setSpacing(20);
            //components.getChildren().add(selectPageHBox);

            sP.setContent(components);
            page.setContent(sP);

            pageCount++;

            pageTabs.getTabs().add(page);
            pageList.add(page);
            pageComp.add(components);
            printPageList();

            comfirmSelect.showAndWait();
            EDITED.set(true);

        });

        /**
         * Remove Page Button Handler
         */
        siteTB.removePageButt.setOnAction(e -> {
            Alert comfirmRemove = new Alert(AlertType.CONFIRMATION);
            comfirmRemove.setTitle("Remove Page");
            comfirmRemove.setHeaderText(null);
            comfirmRemove.setContentText("Do you wish to remove this page?");

            Optional<ButtonType> result = comfirmRemove.showAndWait();
            if (result.get() == ButtonType.OK) {
                int selectedPage = pageTabs.getSelectionModel().getSelectedIndex();
                System.out.println("SELECTED TAB" + pageTabs.getSelectionModel().getSelectedIndex());

                int size = checkBoxList.size();

                
                if(multiSelect != true){
                    System.out.println("Hello? " + pageList.get(pageTabs.getSelectionModel().getSelectedIndex()).getId());
                    
                    pageData.remove(pageList.get(selectedPage));
                    pageList.remove(selectedPage);
                    
                    
                    pageTabs.getTabs().remove(selectedPage);

                    pageComp.remove(selectedPage);
                    //checkBoxList.remove(selectedPage - 1);
                    EDITED.set(true);
                }
                

            } else {
                // ... user chose CANCEL or closed the dialog
            }

        });

        /**
         * select a Page Button Handler
         */
        siteTB.selectPageButt.setOnAction(e -> {
            Alert comfirmSelect = new Alert(Alert.AlertType.INFORMATION);
            comfirmSelect.setTitle("Multi Select");
            comfirmSelect.setHeaderText(null);
            comfirmSelect.setContentText("You Can Now Select multiple pages for Deletion.");

            comfirmSelect.showAndWait();

            //enable checkboxes
            int size = checkBoxList.size();
            if (size > 0) {
                if (multiSelect == true) {
                    for (int i = 0; i < size; i++) {

                        checkBoxList.get(i).setDisable(false);
                        multiSelect = false;
                    }
                } else { //disable checkBoxes
                    for (int i = 0; i < size; i++) {
                        checkBoxList.get(i).setSelected(false);
                        checkBoxList.get(i).setDisable(true);
                        multiSelect = true;
                    }
                }
            }

        });

    }

    /**
     * Initializes a new tab when the New Page Button is pressed
     *
     * @return Tab
     */
    public Tab initTab() {
        Tab tab = new Tab();

        tab.setClosable(false);
        tab.setText("Page " + pageCount);
        tab.setId("Page" + pageCount);

        return tab;
    }

    /**
     * An Event Handler that listens for changes in selected tab and stores the
     * selected tab as selectedTab and the index of the selected tab as
     * selectedTabIndex
     *
     */
    public void initSelectedTab() {

        pageTabs.getSelectionModel().selectedItemProperty().addListener(
                new ChangeListener<Tab>() {
                    @Override
                    public void changed(ObservableValue<? extends Tab> ov, Tab t, Tab t1) {

                        if (selectedComponent != null) {
                            selectedComponent.getStyleClass().remove(CSS_SELECTEDCOMP);
                            selectedComponent.getStyleClass().add(CSS_COMPONENT);
                        }

                        selectedTab = t1;
                        selectedComponent = null;
                        editButt.setDisable(true);
                        removeButt.setDisable(true);
                        selectedTabIndex = pageTabs.getSelectionModel().getSelectedIndex();
                        
                        Page tabData = pageData.get(t1);
                        try{
                            if(tabData.getBannerFile() != null){
                            bannerFileName.setText(tabData.getBannerFile().getName());
                        }else{
                            bannerFileName.setText("");
                        }
                        }catch(NullPointerException NE){
                            
                        }

                        if (pageTabs.getTabs().size() > 0) {
                            pageTitleField.setText(t1.getText());
                        }

                        if (pageTabs.getSelectionModel().isSelected(0)) {
                            removePageButt.setDisable(true);
                            pageTitleField.setDisable(true);
                        } else {
                            removePageButt.setDisable(false);
                            pageTitleField.setDisable(false);
                        }

                    }
                });
    }
    
    public void printPageList(){
        
        for(int i =0; i < pageList.size(); i++){
            System.out.print(pageList.get(i).getText() + " , ");
        }
        System.out.println();
    }

}
