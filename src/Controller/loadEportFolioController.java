package Controller;

import static Controller.SiteTbController.pageCount;
import static Controller.WorkspaceToolBar_controller.selectedComponent;
import Page.Component;
import Page.Page;
import static Page.PageList.componentMap;
import static Page.PageList.pageComp;
import static Page.PageList.pageData;
import static Page.PageList.pageList;
import static View.SiteEditorView.pageTabs;
import static View.WorkspaceToolBar_view.bannerFileName;
import static View.WorkspaceToolBar_view.bannerTxtField;
import static View.WorkspaceToolBar_view.editButt;
import static View.WorkspaceToolBar_view.removeButt;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENT;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENTTYPE;
import static eportfoliogenerator.StartupConstantsEport.CSS_SELECTEDCOMP;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author JL
 */
class loadEportFolioController {

    public loadEportFolioController(File fileToLoad) throws FileNotFoundException {

        try {

            FileReader fReader = new FileReader(fileToLoad.getAbsolutePath());
            JSONObject ePortJson = (JSONObject) new JSONParser().parse(fReader);
            System.out.println(ePortJson.size());
            String fileName = fileToLoad.getName();
            fileName = fileName.substring(0, fileName.length() - 5);
            System.out.println("fileName : " + fileName);

            JSONArray pageObject = (JSONArray) ePortJson.get(fileName);

            constructPageList(pageObject);

        } catch (IOException ex) {
            Logger.getLogger(loadEportFolioController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(loadEportFolioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void constructPageList(JSONArray pageArray) {

        pageCount = pageArray.size() + 1;

        for (int i = 0; i < pageArray.size(); i++) {
            JSONObject pageObj = (JSONObject) pageArray.get(i);
            Iterator it = pageObj.entrySet().iterator();
            Map.Entry pair = (Map.Entry) it.next();
            System.out.println("KEY : " + pair.getKey() + " VALUE : " + pair.getValue());

            Tab pageTab = initTab(pair.getKey().toString());

            //for each page construct the pageData
            JSONObject pageInfo = (JSONObject) pair.getValue();

            Page pageToAdd = constructPageData(pageInfo, i);

            pageData.put(pageTab, pageToAdd);
        }

    }

    public Page constructPageData(JSONObject pageinfo, int pageIndex) {

        Page page = new Page();

        Iterator it = pageinfo.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry pagePair = (Map.Entry) it.next();
            switch (pagePair.getKey().toString()) {
                case "Components":
                    System.out.println("BUILD COMPONENTS: " + pagePair.getValue().toString());
                    JSONArray componentArray = (JSONArray) pagePair.getValue();
                    VBox boxToSet = constructComponents(componentArray);
                    boxToSet.setSpacing(15);

                    Tab tabToSet = pageList.get(pageIndex);
                    tabToSet.setContent(boxToSet);
                    break;

                case "Layout":
                    System.out.println("SET LAYOUT: " + pagePair.getValue().toString());
                    page.setLayout(pagePair.getValue().toString());
                    break;

                case "FooterTxt":
                    System.out.println("SET FOOTERTXT: " + pagePair.getValue().toString());
                    page.setFooterText(pagePair.getValue().toString());
                    break;

                case "BannerImage":
                    System.out.println("SET BANNERIMAGE: " + pagePair.getValue().toString());
                    page.setBannerImage(pagePair.getValue().toString());
                    //bannerFileName.setText(page.getBannerImage());
                    break;

                case "Color":
                    System.out.println("SET COLOR: " + pagePair.getValue().toString());
                    page.setColor(pagePair.getValue().toString());
                    break;

                case "Font":
                    System.out.println("SET FONT: " + pagePair.getValue().toString());
                    page.setFont(pagePair.getValue().toString());
                    break;
                case "StudentName" : 
                    System.out.println("****SET STUDENT NAME"  + pagePair.getValue().toString());
                    bannerTxtField.setText(pagePair.getValue().toString());
                    
            }
        }

        return page;
    }

    public VBox constructComponents(JSONArray compArry) {

        VBox pageBox = new VBox();

        for (int i = 0; i < compArry.size(); i++) {

            JSONObject comp = (JSONObject) compArry.get(i);
            VBox compBoxToAdd = new VBox();
            Label textTypeLabel = new Label(comp.get("Type").toString());
            textTypeLabel.getStyleClass().add(CSS_COMPONENTTYPE);
            compBoxToAdd.getChildren().add(textTypeLabel);
            initSelectHandler(compBoxToAdd);

            compBoxToAdd.getStyleClass().add(CSS_COMPONENT);
            compBoxToAdd.setPrefWidth(1000);

            Component compToAdd = new Component();

            fillComponentData(compToAdd, comp);

            pageBox.getChildren().add(compBoxToAdd);
            componentMap.put(compBoxToAdd, compToAdd);
        }

        pageComp.add(pageBox);

        return pageBox;

    }

    public void fillComponentData(Component compToFill, JSONObject Comp) {

        switch (Comp.get("Type").toString()) {

            case "Paragraph":
                
                compToFill.setComponentType("Paragraph");
                compToFill.setFontSize(Comp.get("Size").toString());
                compToFill.setFont(Comp.get("Font").toString());
                compToFill.setText(Comp.get("Text").toString());
                
                
                break;
                
            case "Header":
                
                compToFill.setComponentType("Header");
                compToFill.setText(Comp.get("Text").toString());
                break;
                
            case "List":
                
                compToFill.setComponentType("List");
                JSONArray listToAdd = (JSONArray)Comp.get("ListItems");
                
                ArrayList<String> listArray = new ArrayList<String>();
                
                if(listToAdd != null){
                    for(int i = 0; i< listToAdd.size(); i++){
                        listArray.add(listToAdd.get(i).toString());
                        System.out.println(" " + listArray.get(i) +" ");
                        
                    }
                    
                }
                
                compToFill.setListItems(listArray);
                
                break;
                
            case "Video":
                
                compToFill.setComponentType("Video");
                compToFill.setVidCaption(Comp.get("Caption").toString());
                File videoFileToAdd = new File(Comp.get("VideoPath").toString());
                compToFill.setVidFile(videoFileToAdd);
                compToFill.setVidHeight(Comp.get("Height").toString());
                compToFill.setVidWidth(Comp.get("Width").toString());
                
                break;
                
            case "Image":
                
                compToFill.setComponentType("Image");
                compToFill.setImgCaption(Comp.get("Caption").toString());
                File ImageToAdd = new File(Comp.get("ImagePath").toString());
                compToFill.setImgFile(ImageToAdd);
                compToFill.setImgHeight(Comp.get("Height").toString());
                compToFill.setImgPosition(Comp.get("Postition").toString());
                compToFill.setImgWidth(Comp.get("Width").toString());
                break;
                
            case "SlideShow":
                
                compToFill.setComponentType("SlideShow");
                compToFill.setSlideShowFilePath(Comp.get("SlideShowFile").toString());
                compToFill.setSlideShowFile(new File(Comp.get("SlideShowFile").toString()));
                break;

        }

    }

    public void initSelectHandler(VBox componentBox) {

        componentBox.setOnMouseClicked(e -> {
            System.out.println("A component is SELECTED");

            //SELECT COMPONENT IF NONE
            if (selectedComponent == null) {
                selectedComponent = componentBox;
                componentBox.getStyleClass().remove(CSS_COMPONENT);
                componentBox.getStyleClass().add(CSS_SELECTEDCOMP);
                removeButt.setDisable(false);
                editButt.setDisable(false);

            }

            if (componentBox != selectedComponent) {
                //DESELECT COMPONENT and SELECT NEW
                selectedComponent.getStyleClass().remove(CSS_SELECTEDCOMP);
                selectedComponent.getStyleClass().add(CSS_COMPONENT);
                selectedComponent = componentBox;
                componentBox.getStyleClass().remove(CSS_COMPONENT);
                componentBox.getStyleClass().add(CSS_SELECTEDCOMP);
            }

        });
    }

    public Tab initTab(String pageName) {
        Tab page = new Tab();
        page.setClosable(false);
        page.setText(pageName);

        pageTabs.getTabs().add(page);
        pageList.add(page);

        return page;
    }
}
