package View;

import static eportfoliogenerator.StartupConstantsEport.ADDICONPATH;
import static eportfoliogenerator.StartupConstantsEport.CSS_PAGEPANE;
import static eportfoliogenerator.StartupConstantsEport.CSS_SITEPANE;
import static eportfoliogenerator.StartupConstantsEport.REMOVEICONPATH;
import static eportfoliogenerator.StartupConstantsEport.SELECTICONPATH;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class SiteToolbar_view {

    public static Button addPageButt = new Button();
    public static Button removePageButt = new Button();
    public static Button selectPageButt = new Button();

    public static String STUDENTNAME;

    public VBox siteBar = new VBox();
    public ToolBar siteToolbar = new ToolBar();

    
    

    public SiteToolbar_view() {
        initButton(addPageButt, ADDICONPATH);
        initButton(removePageButt, REMOVEICONPATH);
        initButton(selectPageButt, SELECTICONPATH);
        
        addPageButt.setTooltip(new Tooltip("Add New Page"));
        removePageButt.setTooltip( new Tooltip("Remove Page(s)"));
        selectPageButt.setTooltip(new Tooltip("Select Pages"));

        initSiteFlowPane();
        
        //STYLE CLASSES
        siteToolbar.getStyleClass().add(CSS_SITEPANE);
        

        initSiteBar();
    }

    private void initButton(Button button, String iconPath) {

        Image buttIcon = new Image("file:" + iconPath);
        button.setGraphic(new ImageView(buttIcon));
        button.setVisible(true);
        button.setDisable(true);

    }

    private void initSiteFlowPane() {
        siteToolbar.getItems().add(addPageButt);
        siteToolbar.getItems().add(removePageButt);
       // siteToolbar.getItems().add(selectPageButt);
    }

    private void initSiteBar() {
        siteBar.getChildren().add(siteToolbar);

        
    }



    public VBox getSiteBar() {
        return siteBar;
    }
}
