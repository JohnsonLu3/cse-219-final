package View;

import Model.FileToolBar_model;
import Model.SiteToolBar_model;
import Model.WorkSpaceMode_model;
import Model.WorkspaceToolBar_model;
import static eportfoliogenerator.StartupConstantsEport.UICSSPATH;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JL
 */
public class Eport_view {

    //Panes and Nodes
    public static Stage primaryStage;
    private Scene primScene;
    public static BorderPane mainPane = new BorderPane();
    private VBox topToolsPane = new VBox();
    private VBox sideToolsPane = new VBox();
    
   


    public static Pane viewMode;

    //Toolbars
    FileToolBar_model fileTbModel = new FileToolBar_model();

    public Eport_view(Stage pStage) {

        //Set stage
        primaryStage = pStage;
        mainPane = initPane();

        //Set Scene
        primScene = new Scene(mainPane);
        primScene.getStylesheets().add(UICSSPATH);
        primaryStage.setScene(primScene);
        primaryStage.setHeight(800);
        primaryStage.setWidth(1280);
        primaryStage.show();
        
    }

    public BorderPane initPane() {
        BorderPane workSpacePane = new BorderPane();

        //set Top Tools Pane
        FileToolBar_model fileToolBar = new FileToolBar_model();
        ToolBar FTB = fileToolBar.getFileTB();

        SiteToolBar_model siteToolBar = new SiteToolBar_model();
        VBox STB = siteToolBar.getSiteTB();

        topToolsPane.getChildren().add(FTB);
        topToolsPane.getChildren().add(STB);

        //set sideTools Pane
        sideToolsPane.getChildren().add(new Label("SIDE TOOLS"));

        //set WorkSpace Pane
        WorkspaceToolBar_model workSpace = new WorkspaceToolBar_model();
        ToolBar WSTB = workSpace.getWorkSpaceTB();

        //set Childern
        workSpacePane.setTop(topToolsPane);
        workSpacePane.setLeft(WSTB);

        //CENTER
        VBox mainWorkSpacePane = new VBox();

        WorkSpaceMode_model WSMM = new WorkSpaceMode_model();
        mainWorkSpacePane.getChildren().add(WSMM.getTabPane());
        workSpacePane.setCenter(mainWorkSpacePane);

        return workSpacePane;
    }


}
