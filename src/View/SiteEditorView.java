package View;


import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENT;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENTCONT;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMPONENTTYPE;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class SiteEditorView {
    
    Pane siteEditor = new Pane();
    ScrollPane siteScroll = new ScrollPane();
    VBox components = new VBox();
    VBox workSpacePane = new VBox();
    
    public static TabPane pageTabs = new TabPane();
    
    public SiteEditorView(){
        
        
        //siteScroll.setFitToHeight(true);
        siteScroll.setFitToWidth(true);
        siteScroll.setContent(components);
        siteScroll.setPadding(new Insets(15,0 ,0, 0));
        
        VBox.setVgrow(pageTabs, Priority.ALWAYS);
        pageTabs.setPrefWidth(3000);

        
        workSpacePane.getChildren().add(pageTabs);
        //workSpacePane.getChildren().add(siteScroll);
        siteEditor.getChildren().add(workSpacePane);
        
    }
    
    public void addComponent(){
        
    }
    
    public void removeComponent(){
    
    }
    
    public Pane getSiteEditor(){
        return siteEditor;
    }
    
   
    
    public void initPageTabs(Tab page){
    
        pageTabs.getTabs().add(page);
        page.setContent(siteScroll);
    }
    
    public void initComponent(String componentType){
    
        HBox component = new HBox();
        Label type = new Label(componentType);
        components.getChildren().add(component);
        
    }
    
    
    
    
}
