package View.ToolPrompts;

import View.DialogTemplate;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMFRIMBUTTONS;
import java.io.File;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author JL
 */
public class ImagePrompt {

    public DialogTemplate imageDialog = new DialogTemplate();

    String imageName = "";
    String imageHeight = "600";
    String imageWidth = "800";
    String position = "center";
    String imageCaption = "";
    File imageFile;

    TextField height = new TextField();
    TextField width = new TextField();
    public Button addImage = new Button("Choose Image...");

    public HBox buttons = new HBox();
    public Button okay = new Button("Okay");
    public Button cancel = new Button("Cancel");

    HBox deminsionsBox = new HBox();

    ToggleGroup group = new ToggleGroup();
    RadioButton Left = new RadioButton();
    RadioButton None = new RadioButton();
    RadioButton Right = new RadioButton();

    Label imagePathLabel = new Label("");

    HBox captionBox = new HBox();
    Label captionLabel = new Label("Caption : ");
    TextField captionField = new TextField();

    public String imageEditor() {

        height.setPromptText("Height");
        width.setPromptText("Width");

        imageDialog.setStageTitle("Add Image Component");
        imageDialog.setHeader("Choose a Image to add");
        deminsionsBox.getChildren().add(addImage);
        deminsionsBox.getChildren().add(height);
        //deminsionsBox.getChildren().add(new Label("X"));
        deminsionsBox.getChildren().add(width);
        deminsionsBox.setAlignment(Pos.CENTER);
        deminsionsBox.setSpacing(5);
        deminsionsBox.setPadding(new Insets(20, 0, 0, 0));

        //RADIO BUTTONS
        HBox radioButtons = new HBox();

        Label leftLabel = new Label("Left");
        Label centerLabel = new Label("None");
        Label rightLabel = new Label("Right");

        leftLabel.setPadding(new Insets(0, 15, 0, 0));
        centerLabel.setPadding(new Insets(0, 15, 0, 0));
        rightLabel.setPadding(new Insets(0, 15, 0, 0));

        //Added the radio buttons to a togglable group
        Left.setToggleGroup(group);
        None.setToggleGroup(group);
        Right.setToggleGroup(group);
        None.setSelected(true);

        radioButtons.getChildren().add(Left);
        radioButtons.getChildren().add(leftLabel);
        radioButtons.getChildren().add(None);
        radioButtons.getChildren().add(centerLabel);
        radioButtons.getChildren().add(Right);
        radioButtons.getChildren().add(rightLabel);
        radioButtons.setAlignment(Pos.CENTER);
        radioButtons.setPadding(new Insets(15, 0, 15, 0));
        initRadioHandlers();
        initFileChooser();

        imageDialog.popUpContainer.getChildren().add(radioButtons);

        imageDialog.popUpContainer.getChildren().add(deminsionsBox);

        imagePathLabel.setText(imageName);
        imagePathLabel.setPadding(new Insets(5,5,5,105));
        imagePathLabel.setAlignment(Pos.CENTER);
        imageDialog.popUpContainer.getChildren().add(imagePathLabel);

        //CAPTIION
        captionBox.setAlignment(Pos.CENTER);
        captionBox.setPadding(new Insets(15, 0, 0, 0));
        captionBox.setSpacing(15);
        captionField.setPrefWidth(300);
        captionBox.getChildren().add(captionLabel);
        captionBox.getChildren().add(captionField);

        imageDialog.popUpContainer.getChildren().add(captionBox);

        buttons.getChildren().add(okay);
        buttons.getChildren().add(cancel);
        buttons.getStyleClass().add(CSS_COMFRIMBUTTONS);
        buttons.setSpacing(20);
        buttons.setAlignment(Pos.CENTER);

        imageDialog.popUpContainer.getChildren().add(buttons);

        imageDialog.showPopUp();

        //ON CLOSE handler
        imageDialog.popUpStage.setOnCloseRequest(e -> {

        });

        return imageName;
    }

    public void initRadioHandlers() {
        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov,
                    Toggle old_toggle, Toggle new_toggle) {
                if (group.getSelectedToggle() == Left) {
                    System.out.println("Left Float");
                    position = "left";
                    ;
                }
                if (group.getSelectedToggle() == None) {
                    System.out.println("No Float");
                    position = "none";

                }
                if (group.getSelectedToggle() == Right) {
                    System.out.println("Right Float");
                    position = "right";

                }
            }
        });
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String name) {
        imageName = name;
    }

    public String getImageHeight() {
        imageHeight = height.getText();
        return imageHeight;
    }

    public String getImageWidth() {
        imageWidth = width.getText();
        return imageWidth;
    }

    public String getType() {
        return "Image";
    }

    public String getPosition() {
        return position;
    }

    public String getImageCaption() {

        imageCaption = captionField.getText();
        return imageCaption;
    }

    public File getFile() {
        return imageFile;
    }

    public void setFile(File file) {
        imageFile = file;
    }

    /**
     * Set the selected radio button to the give position
     *
     * @param selectedPosition
     */
    public void setSelectedPosition(String selectedPosition) {
        String pos = selectedPosition;
        if (pos.equals("left")) {
            Left.setSelected(true);
        } else {
            if (pos.equals("none")) {
                None.setSelected(true);
            } else {
                if (pos.equals("right")) {
                    Right.setSelected(true);
                }
            }
        }
    }

    public void fillHeight(String h) {
        height.setText(h);
    }

    public void fillWidth(String w) {
        width.setText(w);
    }

    public void fillCaption(String cap) {
        captionField.setText(cap);
    }

    public void fillFileLabel() {
        String filePath = imageFile.getName();
        imagePathLabel.setText(filePath);
    }
    
    public void fillFileLabel(String fp){
        imagePathLabel.setText(fp);
    
    }

    private void initFileChooser() {
        addImage.setOnAction(i -> {
            Stage fileStage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Image File");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
            File selectedFile = fileChooser.showOpenDialog(fileStage);
            if (selectedFile != null) {
                setFile(selectedFile);
                fillFileLabel();

            }
        });
    }
}
