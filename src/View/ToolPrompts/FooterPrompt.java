package View.ToolPrompts;

import View.DialogTemplate;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMFRIMBUTTONS;
import java.util.Optional;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class FooterPrompt {

    public DialogTemplate footerEditorDialog = new DialogTemplate();

    String footerText = "";
    String footerResult = "";

    public TextArea footerArea = new TextArea();

    public HBox buttons = new HBox();
    public Button okay = new Button("Okay");
    public Button cancel = new Button("Cancel");

    public String footerEditor(String fT) {
        
        footerText = fT;
        footerArea.setText(fT);
        
        VBox footer = new VBox();
        footer.setPadding(new Insets(10,40,0,40));
        footer.getChildren().add(footerArea);
        
        footerEditorDialog.setStageTitle("Edit footers");
        footerEditorDialog.setHeader("Please Provide the following text for the footer");
        footerEditorDialog.popUpContainer.getChildren().add(footer);

        buttons.getChildren().add(okay);
        buttons.getChildren().add(cancel);
        buttons.getStyleClass().add(CSS_COMFRIMBUTTONS);
        buttons.setSpacing(20);
        buttons.setAlignment(Pos.CENTER);
        footerEditorDialog.popUpContainer.getChildren().add(buttons);
        
        
        footerEditorDialog.popUpStage.setOnCloseRequest(e -> {
            if (footerText.equals(footerResult)) {
                footerEditorDialog.closePopUp();
            } else {
                comfrimExit();
            }
        });

        footerEditorDialog.showPopUp();

        return footerText;
    }

    public String getFooterText() {
        
        footerText = footerArea.getText();

        return footerText;
    }

    public void comfrimExit() {

        Alert comfrimExit = new Alert(AlertType.CONFIRMATION);
        comfrimExit.setTitle("Cancel Confirmation");
        comfrimExit.setHeaderText("Edits to Footer were Made");
        comfrimExit.setContentText("Do you wish to exit without saving the edits?");

        Optional<ButtonType> result = comfrimExit.showAndWait();
        if (result.get() == ButtonType.OK) {
            footerEditorDialog.closePopUp();
        } else {
            comfrimExit.close();
        }
    }

}
