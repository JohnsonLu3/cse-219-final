package View.ToolPrompts;

import View.DialogTemplate;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMFRIMBUTTONS;
import java.io.File;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author JL
 */
public class VideoPrompt {

    public DialogTemplate videoDialog = new DialogTemplate();

    String videoName = "";
    String videoHeight = "600";
    String videoWidth = "800";
    String videoCaption = "";
    File videoFile;

    TextField height = new TextField();
    TextField width = new TextField();

    public Button addVideo = new Button("Choose Video...");

    HBox captionBox = new HBox();
    Label captionLabel = new Label("Caption: ");
    TextField captionField = new TextField();

    Label videoPathLabel = new Label("");

    public HBox buttons = new HBox();
    public Button okay = new Button("Okay");
    public Button cancel = new Button("Cancel");

    HBox deminsionsBox = new HBox();

    public String videoEditor() {

        height.setPromptText("Height");
        width.setPromptText("Width");

        videoDialog.setStageTitle("Add Video Component");
        videoDialog.setHeader("Choose a video to add");
        deminsionsBox.getChildren().add(addVideo);
        deminsionsBox.getChildren().add(height);
        //deminsionsBox.getChildren().add(new Label("X"));
        deminsionsBox.getChildren().add(width);
        deminsionsBox.setAlignment(Pos.CENTER);
        deminsionsBox.setSpacing(5);
        deminsionsBox.setPadding(new Insets(100, 0, 0, 0));

        videoDialog.popUpContainer.getChildren().add(deminsionsBox);

        videoPathLabel.setText(videoName);
        videoPathLabel.setPadding(new Insets(5, 5, 5, 105));
        videoPathLabel.setAlignment(Pos.CENTER);
        videoDialog.popUpContainer.getChildren().add(videoPathLabel);

        //CAPTIION
        captionBox.setAlignment(Pos.CENTER);
        captionBox.setPadding(new Insets(15, 0, 0, 0));
        captionBox.setSpacing(15);
        captionField.setPrefWidth(300);
        captionBox.getChildren().add(captionLabel);
        captionBox.getChildren().add(captionField);

        videoDialog.popUpContainer.getChildren().add(captionBox);

        buttons.getChildren().add(okay);
        buttons.getChildren().add(cancel);
        buttons.getStyleClass().add(CSS_COMFRIMBUTTONS);
        buttons.setSpacing(20);
        buttons.setAlignment(Pos.CENTER);

        videoDialog.popUpContainer.getChildren().add(buttons);
        initFileChooser();

        videoDialog.showPopUp();

        //ON CLOSE handler
        videoDialog.popUpStage.setOnCloseRequest(e -> {

        });

        return videoName;
    }

    public String getVideoName() {
        return videoName;
    }

    public String getVideoHeight() {
        videoHeight = height.getText();

        return videoHeight;
    }

    public String getVideoWidth() {
        videoWidth = width.getText();

        return videoWidth;
    }

    public String getType() {
        return "Video";
    }

    public File getFile() {
        return videoFile;
    }

    public void setFile(File file) {
        videoFile = file;
    }

    public String getVideoCaption() {
        videoCaption = captionField.getText();

        return videoCaption;
    }

    public void fillVideoHeight(String h) {
        height.setText(h);
    }

    public void fillVideoWidth(String w) {
        width.setText(w);
    }

    public void fillVideoCaption(String c) {
        captionField.setText(c);
    }
    
    public void fillFileLabel(){
        String filePath = videoFile.getName();
        videoPathLabel.setText(filePath);
    }
    
    public void fillFileLabel(String fp){
        videoPathLabel.setText(fp);
    }

    public void initFileChooser() {
        addVideo.setOnAction(i -> {
            Stage fileStage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Video File");
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("Video Files", "*.mp4", "*.mov", "*.wmv"));
            File selectedFile = fileChooser.showOpenDialog(fileStage);
            if (selectedFile != null) {
                setFile(selectedFile);
                fillFileLabel();
            }
        });
    }

}
