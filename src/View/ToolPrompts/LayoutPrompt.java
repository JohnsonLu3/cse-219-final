package View.ToolPrompts;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.ChoiceDialog;

/**
 *
 * @author JL
 */
public class LayoutPrompt {
    
    private String layout = "Article";
     /**
     * layoutPicker
     * A prompt that pops up when the users presses the layoutbutton
     * It will give the user a drop down that contains 5 different layouts
     * and when the okay button is clicked it will return the choosen layout
     * and if the cancel button is clicked it will ignore all changes.
     * 
     * @return String layout type 
     */
    public String layoutPicker(String LO) {
        
        layout = LO;
        
        List<String> choices = new ArrayList<>();
        choices.add("Article");
        choices.add("Blog");
        choices.add("Card");
        choices.add("Resume");
        choices.add("SideNav");

        ChoiceDialog<String> dialog = new ChoiceDialog<>(layout, choices);
        dialog.setTitle("Choose Layout");
        dialog.setHeaderText("Pick a Layout");
        dialog.setContentText("Choose a Layout for this page:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            System.out.println("Your choice: " + result.get());
            layout = result.get();
        } else {
            System.out.println("The user hit cancel");
        }

        return layout;
    }
}
