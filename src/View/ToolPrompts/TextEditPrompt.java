package View.ToolPrompts;

import Page.Page;
import static Page.PageList.pageData;
import View.DialogTemplate;
import static View.SiteEditorView.pageTabs;
import static eportfoliogenerator.StartupConstantsEport.ADDICONPATH;
import static eportfoliogenerator.StartupConstantsEport.CSS_COMFRIMBUTTONS;
import static eportfoliogenerator.StartupConstantsEport.CSS_RADIOLABEL;
import static eportfoliogenerator.StartupConstantsEport.CSS_TEXTFIELD;
import static eportfoliogenerator.StartupConstantsEport.REMOVEICONPATH;
import java.util.ArrayList;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class TextEditPrompt {

    public DialogTemplate textEditorDialog = new DialogTemplate();
    String textType = "Paragraph";
    String textResult = "";
    String textFont = "Economica";
    String textSize = "";
    public ArrayList<TextField> listArray = new ArrayList<TextField>();
    public ArrayList<String> listStringArray = new ArrayList<String>();
    public HBox buttons = new HBox();
    public Button okay = new Button("Okay");
    public Button cancel = new Button("Cancel");

    public Button addItem = new Button();
    public Button removeItem = new Button();

    public VBox textPane;
    public VBox listItems = new VBox();
    //RADIO BUTTONS
    ToggleGroup group = new ToggleGroup();
    public RadioButton Paragraph = new RadioButton();
    public RadioButton Header = new RadioButton();
    public RadioButton List = new RadioButton();

    //Paragraph
    public ComboBox fontFamily = new ComboBox();
    public TextField sizeInput = new TextField();
    public TextArea textArea = new TextArea();

    //Header
    public TextField header = new TextField();

    /**
     * Text Editor A prompt that pops up when the user pressed the Text edit
     * Button It will ask for which type of type component the user wants
     * [Paragraph | Header | List] A text field is provided to write the desired
     * contents.
     *
     * @return String of Text
     */
    public String textEditor() {

        textEditorDialog.setStageTitle("Text Editor");
        textEditorDialog.setHeader("Choose which type of Text component you wish to use: ");

        //Added the radio buttons to a togglable group
        Paragraph.setToggleGroup(group);
        Header.setToggleGroup(group);
        List.setToggleGroup(group);
        if (!Paragraph.isSelected() && !Header.isSelected() && !List.isSelected()) {
            Paragraph.setSelected(true);
        }

        //RADIO BUTTONS LABELS
        Label paragraphLabel = new Label("Paragraph");
        Label headerLabel = new Label("Header");
        Label listLabel = new Label("List");

        //Adding the radio buttons and their labels to a HBox
        HBox txtComponents = new HBox();
        txtComponents.getChildren().add(Paragraph);
        txtComponents.getChildren().add(paragraphLabel);
        txtComponents.getChildren().add(Header);
        txtComponents.getChildren().add(headerLabel);
        txtComponents.getChildren().add(List);
        txtComponents.getChildren().add(listLabel);

        txtComponents.setAlignment(Pos.CENTER);

        //Styling the RadioButtons
        paragraphLabel.getStyleClass().add(CSS_RADIOLABEL);
        headerLabel.getStyleClass().add(CSS_RADIOLABEL);
        listLabel.getStyleClass().add(CSS_RADIOLABEL);

        textEditorDialog.popUpContainer.getChildren().add(txtComponents);

        if (Paragraph.isSelected()) {
            textType = "Paragraph";
            textPane = paragraphView();
        }
        if (List.isSelected()) {
            textType = "List";
            textPane = listView();
        }
        if (Header.isSelected()) {
            textType = "Header";
            textPane = headerView();
        }

        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            public void changed(ObservableValue<? extends Toggle> ov,
                    Toggle old_toggle, Toggle new_toggle) {
                if (group.getSelectedToggle() == Paragraph) {
                    System.out.println("Paragraph");
                    textPane = paragraphView();
                    textEditorDialog.popUpContainer.getChildren().set(2, textPane);
                    textType = "Paragraph";
                }
                if (group.getSelectedToggle() == List) {
                    System.out.println("List");
                    textPane = listView();
                    textEditorDialog.popUpContainer.getChildren().set(2, textPane);
                    textType = "List";
                }
                if (group.getSelectedToggle() == Header) {
                    System.out.println("Header");
                    textPane = headerView();
                    textEditorDialog.popUpContainer.getChildren().set(2, textPane);
                    textType = "Header";
                }
            }
        });

        textEditorDialog.popUpContainer.getChildren().add(textPane);

        buttons.getChildren().add(okay);
        buttons.getChildren().add(cancel);
        buttons.getStyleClass().add(CSS_COMFRIMBUTTONS);
        buttons.setSpacing(20);
        buttons.setAlignment(Pos.CENTER);

        textEditorDialog.popUpContainer.getChildren().add(buttons);

        //Method launched when editor is closed
        textEditorDialog.popUpStage.setOnCloseRequest(e -> {
            System.out.println("Dialog was closed");
        });

        textEditorDialog.showPopUp();

        return textResult;
    }

    public String getTextType() {
        return textType;
    }

    public String getTextResult() {
        if (textType.equals("Paragraph")) {
            textResult = textArea.getText();
        }

        if (textType.equals("Header")) {
            textResult = header.getText();
        }
        return textResult;
    }

    public String getTextFont() {
        textFont = (String) fontFamily.getValue();
        System.out.println("FONT ::" + textFont);
        return textFont;
    }

    public String getTextSize() {
        textSize = sizeInput.getText();
        return textSize;
    }

    public VBox paragraphView() {
        VBox paragraphPane = new VBox();

        HBox fontStyle = new HBox();
        Label font = new Label("Font : ");

        Label fontSize = new Label("Size : ");

        fontStyle.setAlignment(Pos.CENTER);
        fontStyle.setPadding(new Insets(0, 0, 10, 0));
        fontSize.setPadding(new Insets(0, 0, 0, 5));
        sizeInput.setPrefWidth(30);

        fontFamily = new ComboBox();
        fontFamily.getItems().addAll("Economica",
                "PoiretOne",
                "ProductSans",
                "Lora",
                "Roboto"
        );

        Page pageTabData = pageData.get(pageTabs.getSelectionModel().getSelectedItem());
        if (fontFamily.getValue() == null) {
            fontFamily.setValue(pageTabData.getFont());
        }

        fontStyle.getChildren().add(font);
        fontStyle.getChildren().add(fontFamily);
        fontStyle.getChildren().add(fontSize);
        fontStyle.getChildren().add(sizeInput);

        paragraphPane.getChildren().add(fontStyle);

        textArea.getStyleClass().add(CSS_TEXTFIELD);
        VBox text = new VBox();
        text.setPadding(new Insets(10, 40, 0, 40));
        text.getChildren().add(textArea);

        paragraphPane.getChildren().add(text);

        textArea.textProperty().addListener((observable, oldValue, newValue) -> {
            textResult = newValue;

        });

        sizeInput.textProperty().addListener((observable, oldValue, newValue) -> {
            textSize = newValue;

        });

        return paragraphPane;
    }

    public VBox listView() {
        VBox listPane = new VBox();

        listArray = new ArrayList<TextField>();
        listStringArray = new ArrayList<String>();

        initButton(addItem, ADDICONPATH);
        initButton(removeItem, REMOVEICONPATH);
        addItem.setTooltip(new Tooltip("Add List Item"));
        removeItem.setTooltip(new Tooltip("Remove List Item"));

        initListHandlers();

        HBox listButtons = new HBox();
        listButtons.getChildren().add(addItem);
        listButtons.getChildren().add(removeItem);
        listPane.getChildren().add(listButtons);
        /**
        if (listItems.getChildren().isEmpty()) {

            TextField firstListItem = listItem();
            listArray.add(firstListItem);
            listItems.getChildren().add(firstListItem);
        }**/

        listItems.setSpacing(5);
        ScrollPane listScroll = new ScrollPane();
        listScroll.setContent(listItems);
        listPane.getChildren().add(listScroll);
        return listPane;

    }

    public VBox headerView() {
        VBox headerPane = new VBox();

        headerPane.getChildren().add(header);
        headerPane.setPadding(new Insets(10, 40, 0, 40));

        header.textProperty().addListener((observable, oldValue, newValue) -> {
            textResult = newValue;

        });

        return headerPane;
    }

    public TextField listItem() {
        TextField item = new TextField();
        item.setPrefWidth(250);

        return item;
    }

    public TextField listItem(String itemData) {
        TextField item = new TextField();
        item.setText(itemData);
        return item;
    }

    private void initButton(Button button, String iconPath) {

        Image buttIcon = new Image("file:" + iconPath);
        button.setGraphic(new ImageView(buttIcon));
        button.setVisible(true);

    }

    public void initListHandlers() {
        addItem.setOnAction(e -> {
            TextField item = listItem();
            listItems.getChildren().add(item);
            listArray.add(item);
        });

        removeItem.setOnAction(e -> {
            int index = listItems.getChildren().size() - 1;
            if (index > 0) {
                listItems.getChildren().remove(index);
                listArray.remove(index);
            }
        });
    }

    //Call this when okay Button is hit
    public void setListItems() {

        int size = listItems.getChildren().size();
      
        
        for (int i = 0; i < size; i++) {

            String listItemText = listArray.get(i).getText();
            listStringArray.add(listItemText);

        }

    }

    public void fillList(ArrayList<String> listData) {

        int size = listData.size();

        for (int i = 0; i < size; i++) {
            
            TextField listItemField = listItem(listData.get(i));
            listArray.add(listItemField);
            listItems.getChildren().add(listItemField);
        }

    }

    public ArrayList<String> getListItems() {
        return listStringArray;
    }
}
