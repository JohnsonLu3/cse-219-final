package View.ToolPrompts;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.ChoiceDialog;

/**
 *
 * @author JL
 */
public class FontPrompt {
    
    
    
    String font = "Economica";
    
    
    
    /**
     * fontPicker A prompt that pops up when the users presses the font button
     * It will give the user a drop down that contains 5 different fonts for the
     * page of the ePortfolio. Hitting okay will return the picked font and the
     * cancel button will ignore any actions.
     *
     * @return String Font that the user wishes to use
     */
    public String fontPicker(String FC) {
        
        font = FC;
        
        List<String> choices = new ArrayList<>();
        choices.add("Economica");
        choices.add("PoiretOne");
        choices.add("ProductSans");
        choices.add("Lora");
        choices.add("Roboto");

        ChoiceDialog<String> dialog = new ChoiceDialog<>(font, choices);
        dialog.setTitle("Choose Color");
        dialog.setHeaderText("Pick a Font");
        dialog.setContentText("Choose a Font Scheme for this page:");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            System.out.println("Your choice: " + result.get());
            font = result.get();
        } else {
            System.out.println("The user hit cancel");
        }

        return font;
    }

}
