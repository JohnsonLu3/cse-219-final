package View;

import static eportfoliogenerator.StartupConstantsEport.CSS_HEADER;
import static eportfoliogenerator.StartupConstantsEport.POPUPCSSPATH;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author JL
 */
public  class DialogTemplate {
    
    public Stage popUpStage = new Stage();
    public Scene popUpScene;
    public HBox header = new HBox();
    public VBox  popUpContainer = new VBox();
    
    public DialogTemplate(){
        popUpStage.setHeight(500);
        popUpStage.setWidth(650);
        
        popUpScene = new Scene(popUpContainer);
        
        //CSS
        popUpScene.getStylesheets().add(POPUPCSSPATH);
        header.getStyleClass().add(CSS_HEADER);
        

        popUpStage.setScene(popUpScene);
        
        
        
    }
    
    public void setStageTitle(String title){
    
        popUpStage.setTitle(title);
    }
    
    public void setHeader(String headTxt){
        
        Label headerText = new Label(headTxt);
        header.getChildren().add(headerText);
        popUpContainer.getChildren().add(header);
    }
    
    public void setContains(VBox contents){
        popUpContainer.getChildren().add(contents);
    }
    
    public void showPopUp(){
        popUpStage.show();
    }
    
    public void closePopUp(){
        popUpStage.close();
        System.out.println("prompt closed");
    }
    
}
