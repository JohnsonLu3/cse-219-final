package View;

import Controller.EnableButtons;
import static View.SiteEditorView.pageTabs;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

/**
 *
 * @author JL
 */
public class WorkSpaceMode_view {

    public static TabPane modeTabPane = new TabPane();
    public Tab sitetab = new Tab();
    public Tab editortab = new Tab();

    public WorkSpaceMode_view() {

        editortab.setText("Editor View");
        sitetab.setText("Site View");

        editortab.setClosable(false);
        sitetab.setClosable(false);

        modeTabPane.getTabs().add(editortab);
        modeTabPane.getTabs().add(sitetab);
        
        

    }

    public TabPane getTabPane() {
        return modeTabPane;
    }



}
