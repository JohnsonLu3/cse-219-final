package View;

import static Controller.FileController.EDITED;
import static View.SiteEditorView.pageTabs;
import static eportfoliogenerator.StartupConstantsEport.COLORICONPATH;
import static eportfoliogenerator.StartupConstantsEport.CSS_EDITORHEADER;
import static eportfoliogenerator.StartupConstantsEport.CSS_PAGEEDITOR;
import static eportfoliogenerator.StartupConstantsEport.CSS_WORKSPACE;
import static eportfoliogenerator.StartupConstantsEport.DELETEICONPATH;
import static eportfoliogenerator.StartupConstantsEport.EDITICONPATH;
import static eportfoliogenerator.StartupConstantsEport.FONTICONPATH;
import static eportfoliogenerator.StartupConstantsEport.IMGICONPATH;
import static eportfoliogenerator.StartupConstantsEport.LAYOUTICONPATH;
import static eportfoliogenerator.StartupConstantsEport.LINKICONPATH;
import static eportfoliogenerator.StartupConstantsEport.REMOVEICONPATH;
import static eportfoliogenerator.StartupConstantsEport.SLIDEICONPATH;
import static eportfoliogenerator.StartupConstantsEport.TEXTICONPATH;
import static eportfoliogenerator.StartupConstantsEport.VIDEOICONPATH;
import javafx.geometry.Rectangle2D;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;

/**
 *
 * @author JL
 */
public class WorkspaceToolBar_view{

    public VBox workSpacePane = new VBox();
    private Label pageEditor = new Label("Page Editor");

    private Label pageTitle = new Label("Page Title");
    public static TextField pageTitleField = new TextField();
    public static String pageName;

    private Label bannerTxt = new Label("Student Name: ");
    public static TextField bannerTxtField = new TextField();
    public static String bannerText;

    private Label chooseBanner = new Label("Banner Image: ");
    public static Button bannerImage = new Button("Choose File...");
    public static Label bannerFileName = new Label(""); 

    //Page Style
    private GridPane stylePane = new GridPane();
    private Label pageStyle = new Label("Page Style : ");
    public static Button layoutButt = new Button();
    public static Button colorButt = new Button();
    public static Button fontButt = new Button();

    //Page Components
    private GridPane componentPane = new GridPane();
    private Label pageComponents = new Label("Page Components : ");
    public static Button textButt = new Button();
    public static Button imgButt = new Button();
    public static Button slideButt = new Button();
    public static Button videoButt = new Button();
    public static Button linkButt = new Button();
    

    //Component Edit
    private GridPane editorPane = new GridPane();
    private Label componentEditor = new Label("Component Edit Tools : ");
    public static Button editButt = new Button();
    public static Button removeButt = new Button();
    
    //Footer
    public Label footerLabel = new Label("Footer : ");
    public static Button footer = new Button("Add Footer Text");
    
    
    ToolBar workspaceToolBar = new ToolBar();
    Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
    
    //WORKSPACE BUTTONS
    //Button colorButton()
    //END OF WORKSPACE BUTTONS
    public static String BANNERTEXT = "";

    public WorkspaceToolBar_view() {

        initWorkSpace();

        initTxtField();
        
        
        bannerTxtField.setDisable(true);
        pageTitleField.setDisable(true);
        bannerImage.setDisable(true);
        footer.setDisable(true);
        
        //init Buttons
        initButton(layoutButt, LAYOUTICONPATH);
        initButton(colorButt, COLORICONPATH);
        initButton(fontButt, FONTICONPATH);
        initButton(textButt, TEXTICONPATH);
        initButton(imgButt, IMGICONPATH);
        initButton(slideButt, SLIDEICONPATH);
        initButton(videoButt, VIDEOICONPATH);
        initButton(linkButt, LINKICONPATH);
        initButton(editButt, EDITICONPATH);
        initButton(removeButt, DELETEICONPATH);
        
        
        layoutButt.setTooltip( new Tooltip("Choose A Layout"));
        colorButt.setTooltip( new Tooltip("Choose A Color Scheme"));
        fontButt.setTooltip( new Tooltip("Choose A Font Scheme"));
        textButt.setTooltip( new Tooltip("Add Text Component"));
        imgButt.setTooltip( new Tooltip("Add Image Component"));
        slideButt.setTooltip( new Tooltip("Add SlideShow Component"));
        videoButt.setTooltip( new Tooltip("Add Video Component"));
        linkButt.setTooltip( new Tooltip("Add Link Component"));
        editButt.setTooltip( new Tooltip("Edit Selected Component"));
        removeButt.setTooltip( new Tooltip("Remove Selected Component(s)"));
        
        workspaceToolBar.setPrefHeight(primaryScreenBounds.getHeight());
        workspaceToolBar.getItems().add(workSpacePane);
        
        workSpacePane.getStyleClass().add(CSS_WORKSPACE);
        pageEditor.getStyleClass().add(CSS_PAGEEDITOR);
        
        
    }

    private void initButton(Button button, String iconPath) {

        Image buttIcon = new Image("file:" + iconPath);
        button.setGraphic(new ImageView(buttIcon));
        button.setVisible(true);
        button.setDisable(true);

    }

    private void initWorkSpace() {

        //PAGE
        workSpacePane.getChildren().add(pageEditor);
        workSpacePane.getChildren().add(pageTitle);
        workSpacePane.getChildren().add(pageTitleField);
        workSpacePane.getChildren().add(bannerTxt);
        workSpacePane.getChildren().add(bannerTxtField);
        workSpacePane.getChildren().add(chooseBanner);
        workSpacePane.getChildren().add(bannerImage);
        workSpacePane.getChildren().add(bannerFileName);
        
        workSpacePane.getChildren().add(pageStyle);
        workSpacePane.getChildren().add(stylePane);
        workSpacePane.getChildren().add(pageComponents);
        workSpacePane.getChildren().add(componentPane);
        workSpacePane.getChildren().add(componentEditor);
        workSpacePane.getChildren().add(editorPane);
        workSpacePane.getChildren().add(footerLabel);
        workSpacePane.getChildren().add(footer);
        initSubSections();
        initLabelStyle();
        
        

    }

    private void initTxtField() {

        bannerTxtField.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("textfield changed from " + oldValue + " to " + newValue);
            bannerText = newValue;
           
            EDITED.set(true);
        });

        pageTitleField.textProperty().addListener((observable, oldValue, newValue) -> {
            System.out.println("textfield changed from " + oldValue + " to " + newValue);
            int tabIndex = pageTabs.getSelectionModel().getSelectedIndex();
            
            pageTabs.getTabs().get(tabIndex).setText(newValue);
            EDITED.set(true);
        });

    }

    private void initSubSections() {

        //STYLE
        GridPane.setConstraints(layoutButt, 1, 1);
        GridPane.setConstraints(colorButt, 2, 1);
        GridPane.setConstraints(fontButt, 3, 1);

        stylePane.getChildren().add(layoutButt);
        stylePane.getChildren().add(colorButt);
        stylePane.getChildren().add(fontButt);

        //Components
        GridPane.setConstraints(textButt, 1, 1);
        GridPane.setConstraints(imgButt, 2, 1);
        GridPane.setConstraints(slideButt, 3, 1);
        GridPane.setConstraints(videoButt, 1, 2);
        GridPane.setConstraints(linkButt, 2, 2);

        componentPane.getChildren().add(textButt);
        componentPane.getChildren().add(imgButt);
        componentPane.getChildren().add(slideButt);
        componentPane.getChildren().add(videoButt);
        componentPane.getChildren().add(linkButt);

        //EDITORS
        GridPane.setConstraints(editButt, 1,1);
        GridPane.setConstraints(removeButt, 2, 1);

        editorPane.getChildren().add(editButt);
        editorPane.getChildren().add(removeButt);
    }
    
    public void initLabelStyle(){
        pageStyle.getStyleClass().add(CSS_EDITORHEADER);
        pageComponents.getStyleClass().add(CSS_EDITORHEADER);
        componentEditor.getStyleClass().add(CSS_EDITORHEADER);
        footerLabel.getStyleClass().add(CSS_EDITORHEADER);
    }

    public ToolBar getWorkSpace() {
        return workspaceToolBar;
    }

}
