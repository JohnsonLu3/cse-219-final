package Page;

import java.io.File;
import javafx.scene.image.Image;

/**
 *
 * @author JL
 */
public class Page {

    String bannerImage = "";
    File bannerFile = null;
   
    String layout = "Article";
    String color = "Albany";
    String font = "Economica";
    static String footerText = "";

    public void setBannerImage(File BI) {
        bannerFile = BI;
        bannerImage = BI.getPath();
    }
    
    public void setBannerImage(String BI){
        bannerImage = BI;
        bannerFile = new File(BI);
    }


    public void setLayout(String layO) {
        layout = layO;
    }

    public void setColor(String clr) {
        color = clr;
    }

    public void setFont(String fnt) {
        font = fnt;
    }
    public void setFooterText(String foot){
        footerText = foot;
    }

    //GETTERS

    public String getBannerImage() {
        return bannerImage;
    }
    
    public File getBannerFile(){
        return bannerFile;
    }



    public String getColor() {
        return color;
    }

    public String getFont() {
        return font;
    }

    public String getLayout() {
        return layout;
    }
    
    public String getFootereTxt(){
        return footerText;
    }
}
