package Page;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tab;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class PageList {
    
    //Holds a list of Tabs(Pages)
    public static ArrayList<Tab> pageList = new ArrayList<Tab>();
    
    public static HashMap<Tab, Page> pageData = new HashMap<Tab, Page>();
    
    //Holds a List of VBox(Page VBox of each page)
    public static ArrayList<VBox> pageComp = new ArrayList<VBox>();
    
    //Holds a list of all the checkboxes excluding the index
    public static ArrayList<CheckBox> checkBoxList = new ArrayList<CheckBox>();
    
    //Holds a Map of components and their index for all components
    public static HashMap<VBox,Component> componentMap = new HashMap<VBox,Component>();
    
    

}
