package Page;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author JL
 */
public class Component {
    /**
     * Such by design I shoud really make this an abstract and then
     * have different components extend this and then they can add their
     * needed data instead of having every component have things they will
     * never use. But I'm pressed with time and this will have to do.
     */
    private String ComponentType;
    //private int index;
    
    //Text
    private String font = "";
    private String fontSize = "";
    private String text = "";
    private ArrayList<String> listItems = new ArrayList<String>();
    
    //Image
    private String imgHeight = "";
    private String imgWidth = "";
    private File imgFile = null;
    private String imgPosition = "";
    private String imgCaption = "";
    
    //Video
    private String vidHeight = "";
    private String vidWidth = "";
    private File vidFile = null;
    private String vidCaption = "";
    
    //slideShow
    private String slideShowFilePath = "";
    private File slideShowFile = null;

    /**
     * @return the ComponentType
     */
    public String getComponentType() {
        return ComponentType;
    }

    /**
     * @param ComponentType the ComponentType to set
     */
    public void setComponentType(String ComponentType) {
        this.ComponentType = ComponentType;
    }

    /**
     * @return the font
     */
    public String getFont() {
        return font;
    }

    /**
     * @param font the font to set
     */
    public void setFont(String font) {
        this.font = font;
    }

    /**
     * @return the fontSize
     */
    public String getFontSize() {
        return fontSize;
    }

    /**
     * @param fontSize the fontSize to set
     */
    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the listItems
     */
    public ArrayList<String> getListItems() {
        return listItems;
    }

    /**
     * @param listItems the listItems to set
     */
    public void setListItems(ArrayList<String> listItems) {
        this.listItems = listItems;
    }

    /**
     * @return the imgHeight
     */
    public String getImgHeight() {
        return imgHeight;
    }

    /**
     * @param imgHeight the imgHeight to set
     */
    public void setImgHeight(String imgHeight) {
        this.imgHeight = imgHeight;
    }

    /**
     * @return the imgWidth
     */
    public String getImgWidth() {
        return imgWidth;
    }

    /**
     * @param imgWidth the imgWidth to set
     */
    public void setImgWidth(String imgWidth) {
        this.imgWidth = imgWidth;
    }

    /**
     * @return the imgPath
     */
    public File getImgFile() {
        return imgFile;
    }

    /**
     * @param imgPath the imgPath to set
     */
    public void setImgFile(File imgFile) {
        this.imgFile = imgFile;
    }

    /**
     * @return the imgPosition
     */
    public String getImgPosition() {
        return imgPosition;
    }

    /**
     * @param imgPosition the imgPosition to set
     */
    public void setImgPosition(String imgPosition) {
        this.imgPosition = imgPosition;
    }

    /**
     * @return the vidHeight
     */
    public String getVidHeight() {
        return vidHeight;
    }

    /**
     * @param vidHeight the vidHeight to set
     */
    public void setVidHeight(String vidHeight) {
        this.vidHeight = vidHeight;
    }

    /**
     * @return the vidWidth
     */
    public String getVidWidth() {
        return vidWidth;
    }

    /**
     * @param vidWidth the vidWidth to set
     */
    public void setVidWidth(String vidWidth) {
        this.vidWidth = vidWidth;
    }

    /**
     * @return the vidPath
     */
    public File getVidFile() {
        return vidFile;
    }

    /**
     * @param vidFile the vidPath to set
     */
    public void setVidFile(File vidFile) {
        this.vidFile = vidFile;
    }

    /**
     * @return the slideShowFilePath
     */
    public String getSlideShowFilePath() {
        return slideShowFilePath;
    }

    /**
     * @param slideShowFilePath the slideShowFilePath to set
     */
    public void setSlideShowFilePath(String slideShowFilePath) {
        this.slideShowFilePath = slideShowFilePath;
    }
    
    public void setSlideShowFile(File slideFile){
    
        slideShowFile = slideFile;
    }
    
    public File getSlideShowFile(){
        return slideShowFile;
    }

    /**
     * @return the imgCaption
     */
    public String getImgCaption() {
        return imgCaption;
    }

    /**
     * @param imgCaption the imgCaption to set
     */
    public void setImgCaption(String imgCaption) {
        this.imgCaption = imgCaption;
    }

    /**
     * @return the vidCaption
     */
    public String getVidCaption() {
        return vidCaption;
    }

    /**
     * @param vidCaption the vidCaption to set
     */
    public void setVidCaption(String vidCaption) {
        this.vidCaption = vidCaption;
    }
    
}
