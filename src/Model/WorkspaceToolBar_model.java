package Model;

import Controller.WorkspaceToolBar_controller;
import View.WorkspaceToolBar_view;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.VBox;

/**
 *
 * @author JL
 */
public class WorkspaceToolBar_model {
    
    private WorkspaceToolBar_view workspaceTB = new WorkspaceToolBar_view();
    private WorkspaceToolBar_controller worksapceController = new WorkspaceToolBar_controller(workspaceTB);
    
    public WorkspaceToolBar_model(){
    
    }
    
    public ToolBar getWorkSpaceTB(){
        return workspaceTB.getWorkSpace();
    }
    
}
