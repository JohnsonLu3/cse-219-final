package Model;

import Controller.FileTbController;
import View.FileToolbar_view;
import javafx.scene.control.ToolBar;

/**
 *
 * @author JL
 */
public class FileToolBar_model {
    
    private FileToolbar_view fileTB = new FileToolbar_view();
    private FileTbController fileController = new FileTbController(fileTB);
    
    public FileToolBar_model(){
        
    }
    
    public ToolBar getFileTB(){
        
        return fileTB.getToolBar();
    }
}
