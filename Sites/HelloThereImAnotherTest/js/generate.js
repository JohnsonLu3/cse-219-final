/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var keyQueue = [];
var componentQueue = [];
var page;
var docName;
var sectionCount = 0;
var pageRead = false;

var head = document.getElementsByTagName('head')[0];
var linkLO = document.createElement('link');
var linkColor = document.createElement('link');
var linkFont = document.createElement('link');
var page = [];


$(function () {

    docName = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
    
    console.log(docName);
    //set up css links
    setUpLinks();

    page = document.getElementById("content");
    page.innerHTML = "";

    $.getJSON('js/ePortFolio.json' , function(data){
        
        console.log("Helllo");
        console.log(data);
        $.each(data, function(ePortfolio, htmlName){
            $.each(htmlName , function(htmlName, pageObj){
               console.log(pageObj + " :: " + htmlName); 
                $.each(pageObj , function(pageName,V){
                    console.log(pageName + " ||| " + V);
   
                    
                    console.log(docName +" *****s");
                    if(pageName == docName && pageRead == false){
                        pageRead = true;
                        console.log("Current Page is " + docName);
                        
                         $.each(V, function(pageDataName , pageData){
                             console.log(pageDataName);
                             lookUpComponent(pageDataName , pageData);
                        });
                    }
                })
            });
        })
        
    });
    

    
    page.innerHTML += "</section>";
    page.innerHTML += "</div>";

});




/**
 * Sets up CSS file links
 * @returns {undefined}
 */
function setUpLinks() {

    linkLO.rel = 'stylesheet';
    linkLO.type = 'text/css';
    linkLO.href = 'css/layout/';
    linkLO.media = 'all';

    linkColor.rel = 'stylesheet';
    linkColor.type = 'text/css';
    linkColor.href = 'css/colors/';
    linkColor.media = 'all';

    linkFont.rel = 'stylesheet';
    linkFont.type = 'text/css';
    linkFont.href = 'css/fonts/';
    linkFont.media = 'all';
}

/**
 * Sets up Compontents using Json values from the key "Components"
 * @returns {undefined}
 */
function setUpComponents() {
    console.log("KeyQueue Length :  " + keyQueue.length);
    queueLength = keyQueue.length;
    for (var i = 0; i < queueLength; i++) {
        
        var componentNum = "";
        var componentInfo = "";
        
        componentNum = keyQueue.shift();
        componentInfo = componentQueue.shift();
        
        page.innerHTML += "<div id=\"section"+ componentNum +"\" class=\"section\">" + componentInfo +"</div>";
        

    }
}

/**
 * This function uses a switch statement and key value pairs
 * to find if the value is a layout, color, font, Nav, or ECT.
 * if the value is a Component then the values of the Component
 * object is then pushed onto a queue for setUpComponents()
 * @param {type} cType
 * @param {type} cContent
 * @returns {undefined}
 */
function lookUpComponent( cType , cContent){
    
    type = cType;
    content = cContent;
    
    switch (type) {
                        case 'Layout':
                            linkLO.href += content + "LO.css";
                            head.appendChild(linkLO);
                            break;
                        case 'Color':
                            linkColor.href += content + ".css";
                            head.appendChild(linkColor);
                            break;
                        case 'Font' :
                            linkFont.href += content + ".css";
                            head.appendChild(linkFont);
                            break;
                        case 'Nav' :
                            page.innerHTML += "<div id=\"content\">";
                            page.innerHTML += content;
                            break;
                        case 'StudentName' :
                            page.innerHTML += "<div id=\"banner\"><h1>" + content + "</h1></div>";
                            break;
                        case 'BannerImage' :
                            page.innerHTML += "<img id=\"bannerImage\" src=img/" + content + "  alt=\"BannerImage\" id=\"bannerImage\" />";
                            break;
                        case 'Components' :
                            
                            page.innerHTML += "<section>";

                            $.each(content, function (Key, Value) {
                                $.each(Value, function(compType, compData){
                                    console.log("compyType : " + compType);
                                    console.log("compData : " + compData);
                                    if(compType === "Type"){
                                        
                                        console.log("*** " + compData);
                                        if(compData === "Paragraph"){
                                            buildParagraph(Value);
                                        }
                                        if(compData === "Header"){
                                            buildHeader(Value);
                                        }
                                        if(compData === "List"){
                                        
                                        }
                                        if(compData === "Video"){
                                            buildVideo(Value);
                                        }
                                        if(compData === "Image"){
                                            buildImg(Value);
                                        }
                                        if(compData === "SlideShow"){
                                           
                                        }
                                        
                                        
                                        
                                    }
                                });
                                
                                console.log("END");

                            });

                            setUpComponents();

                            break;
                        case 'FooterTxt' :
                            console.log(content + "/////");
                            page.innerHTML += "<footer>" + content + "</footer>";
                            break;
                        default:
                            break;
                    }
}

function buildParagraph(compKey){
    
    var text = "";
    var font = "";
    var size = "";
    
    $.each(compKey, function(type, data){
    
        if(type === "Text"){
            text = data;
            console.log("TEXT ::" + text);
        }
        if(type === "Font"){
            font = data;
            console.log("FONT ::" + font);
        }
        if(type == "Size"){
            size = data;
            console.log("SIZE ::" + size);
        }
        
    });
    
    var paraText= "<p>" + text + "</p>";
    
    page.innerHTML += "<div  \" class=\"section\">" + paraText +"</div>";
}

function buildHeader(compKey){
    var text = "";
    
    $.each(compKey, function(type, data){
    
        if(type === "Text"){
            text = data;
        }
    
    });
    
    var headerText = "<h2>" + text + "</h2>";
    
    page.innerHTML += "<div  \" class=\"section\">" + headerText +"</div>";
    }

function buildVideo(compKey){
    
    var caption = "";
    var filePath = "";
    var height = "";
    var width = "";
    
    $.each(compKey, function(type, data){
        if(type === "VideoPath"){
            filePath = "/video/" + data;
        }
        if(type === "Height"){
            height = data;
        }
        
        if(type == "Width"){
            width = data;
        }
        if(type === "Caption"){
            caption = data;
        }
    });
    
    var vidComp;
    
    if(height === "" || width === ""){
        vidComp = "<video width=" + "320" + " height=" + "240" + " controls>" +
                     "<source src=\" " +  filePath + " \" type=\"video/mp4\">" +
                  "Your browser does not support the video tag.</video>"
        
        page.innerHTML += "<div  \" class=\"section\">" + vidComp +
                        "<caption>" +  caption  + "</caption>" + "</div>";
        
        
    }else{
    
            vidComp = "<video width=" + width + " height=" + height + " controls>" +
                     "<source src=\" " +  filePath + " \" type=\"video/mp4\">" +
                  "Your browser does not support the video tag.</video>"
        
         page.innerHTML += "<div  \" class=\"section\">" + vidComp +
                        "<caption>" +  caption  + "</caption>" + "</div>";
    }

}

function buildImg(compKey){
    
    var caption = "";
    var filePath = "";
    var height = "";
    var width = "";
    var position = "";
    
    $.each(compKey, function(type, data){
        if(type === "ImagePath"){
            filePath = "/img/" + data;
        }
        if(type === "Height"){
            height = data;
        }
        
        if(type == "Width"){
            width = data;
        }
        if(type === "Caption"){
            caption = data;
        }
        if(type == "Position"){
            position = data;
        }
    });
    
    var imgComp;
    
    if(height === "" || width === ""){
        imgComp = "<img src=\" " + filePath + " \">";
        
        page.innerHTML += "<div  \" class=\"section\">" + imgComp +
                        "<caption>" +  caption  + "</caption>" + "</div>";
        
        
    }else{
    
        page.innerHTML += "<div  \" class=\"section\">" + imgComp +
                        "<caption>" +  caption  + "</caption>" + "</div>";
        
    }

}
    
